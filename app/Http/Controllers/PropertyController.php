<?php

namespace App\Http\Controllers;
use App\Page;
use App\Property;
use App\City;
use App\Developer;
use App\Testimonial;

use Illuminate\Http\Request;

class PropertyController extends Controller
{
    public function index($slug){

    }

    public function category($category){

        return view('category_property');
    }

    public function search(Request $request){

        $page = Page::where('id', 9)->get();
        $name = $request->input('property');
        $location = $request->input('location');
        $cities = City::orderBy('id', 'asc')->get();

        $properties = Property::where('title','LIKE','%'.$name.'%')
                                ->orWhere('address', 'LIKE', '%' . $name . '%')
                                ->orWhere('property_type', 'LIKE', '%' . $name . '%')
                                ->orWhere('price', 'LIKE', '%' . $name . '%')
                                ->orWhere('city', 'LIKE', '%' . $name . '%')
                                ->where('status', '=', 'ACTIVE')
                                ->orderBy('order', 'asc')
                                ->paginate(9);
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();
        
        return view('search_result', compact('page', 'cities', 'properties', 'testimonials', 'developers'));
    }

    public function property($category, $slug){

        $property = Property::where('slug', '=', $slug )->first();
        $properties =  Property::select('title')->get();
        $relatedProperty = Property::where('city', '=', $property->city )->where('slug', '!=' , $slug)->take(3)->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('property', compact('property', 'properties', 'developers', 'testimonials',  'relatedProperty'));
    }

    public function houseLot(){

        $page = Page::where('id', 3)->get();
        $properties = Property::where('property_type', '=', 'House & Lot')
                            ->where('status', '=', 'ACTIVE')
                            ->orderBy('order', 'asc')
                            ->paginate(9);
        $cities = City::orderBy('city', 'asc')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('category_property', compact('page','properties', 'cities', 'testimonials', 'developers'));
    }

    public function houseLotCity($city){

        $page = Page::where('id', 3)->get();
        $currentCity = $city;
        $properties = Property::where('property_type', '=', 'House & Lot')
                            ->where('city', '=', $city)
                            ->where('status', '=', 'ACTIVE')
                            ->orderBy('order', 'asc')
                            ->paginate(9);

        $cities = City::orderBy('city', 'asc')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('category_property', compact('properties', 'page', 'cities', 'testimonials',  'developers', 'currentCity'));
    }

    public function condominium(){

        $page = Page::where('id', 4)->get();
        $properties = Property::where('property_type', '=', 'Condominium')
                            ->where('status', '=', 'ACTIVE')
                            ->orderBy('order', 'asc')
                            ->paginate(9);
        $cities = City::orderBy('city', 'asc')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('category_property', compact('properties', 'page', 'cities', 'testimonials',  'developers'));
    }

    public function condominiumCity($city){

        $page = Page::where('id', 4)->get();
        $properties = Property::where('property_type', '=', 'Condominium')
                            ->where('city', '=', $city)
                            ->where('status', '=', 'ACTIVE')
                            ->orderBy('order', 'asc')
                            ->paginate(9);

        $cities = City::orderBy('city', 'asc')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('category_property', compact('properties', 'page', 'cities', 'testimonials',  'developers'));
    }

    public function rfo(){

        $page = Page::where('id', 5)->get();
        $properties = Property::where('rfo', '=', 1 )->paginate(6);
        $cities = City::orderBy('city', 'asc')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        return view('rfo', compact('properties', 'page', 'cities', 'testimonials',  'developers'));
    }

    public function rfoType($type){

        $page = Page::where('id', 5)->get();
        $cities = City::orderBy('city', 'asc')->get();
        $testimonials =  Testimonial::all();
        $developers = Developer::orderBy('id', 'asc')->get();

        if ($type == "house-and-lot") {
            $properties = Property::where('property_type', '=', 'House & Lot')
                            ->where('status', '=', 'ACTIVE')
                            ->where('rfo', '=', 1)
                            ->orderBy('order', 'asc')
                            ->paginate(9);
        } elseif($type == "condominium") {
            $properties = Property::where('property_type', '=', 'Condominium')
                            ->where('status', '=', 'ACTIVE')
                            ->where('rfo', '=', 1)
                            ->orderBy('order', 'asc')
                            ->paginate(9);
        } else {
            return redirect('/rfo');
        }

        return view('rfo', compact('properties', 'page', 'cities', 'testimonials',  'developers', 'properties'));
    }
}
