-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: May 26, 2020 at 09:19 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_hac`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image`, `title`, `subtitle`, `order`, `created_at`, `updated_at`) VALUES
(2, 'banners/May2020/j4a9K2jWMgwbNYlguA7q.jpg', 'Casa mira', 'Don\'t judge each day by the harvest you reap - but by the seeds that you plant.', 4, '2020-05-26 01:50:00', '2020-05-26 03:31:16'),
(3, 'banners/May2020/bywEVBB67xQypi53f9jR.jpg', 'Eveything under the sun', 'Rise and Grind.', 3, '2020-05-26 01:51:00', '2020-05-26 04:30:30'),
(4, 'banners/May2020/IdTv2b2QRMts0aR4l7j2.jpg', 'Find your home', 'Sometimes the key to happiness is the key to the right home', 1, '2020-05-26 01:51:00', '2020-05-26 03:20:44'),
(5, 'banners/May2020/JDVp6Y7NAE65m0ofikLt.jpg', 'Vertex', 'Dont wait to buy real estate, buy real estate and wait', 2, '2020-05-26 03:24:00', '2020-05-26 03:31:23');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(2, NULL, 1, 'Category 2', 'category-2', '2020-04-07 00:55:14', '2020-04-07 00:55:14');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city`, `zipcode`, `slug`, `created_at`, `updated_at`, `image`, `featured`) VALUES
(1, 'Lapu-Lapu', '6002', 'lapu-lapu', '2020-04-13 00:04:00', '2020-05-10 00:03:32', 'cities\\May2020\\vwWA1izhy8jXQHC58XTu.jpg', 1),
(2, 'Mandaue', '6014', 'mandaue', '2020-04-13 00:05:00', '2020-05-10 00:00:07', 'cities\\May2020\\A48d4tttyrZiD53TSbES.jpg', 1),
(3, 'Cebu', '6000', 'cebu', '2020-04-13 00:05:00', '2020-05-09 23:58:44', 'cities\\May2020\\Vcgc7RVrpFg69dywnBDU.jpg', 1),
(4, 'Consolacion', '6001', 'consolacion', '2020-04-13 00:08:00', '2020-05-09 23:49:50', 'cities\\May2020\\uvDJ8Ddifh274KHTkDFL.jpg', 1),
(5, 'Liloan', NULL, 'liloan', '2020-04-13 00:23:00', '2020-05-10 06:09:08', 'cities\\May2020\\Z8Eyg6d55SwaFQcwSIfG.jpg', 1),
(6, 'Talisay', NULL, 'talisay', '2020-05-10 06:09:56', '2020-05-10 06:09:56', 'cities\\May2020\\DeBIk8S4sJMW1CCucwxJ.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(265) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(265) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(265) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `message`, `created_at`, `updated_at`, `location`, `property_type`) VALUES
(1, 'Christine G Herda', 'sabrinna@gmail.com', '123', 'Inquiring for Plumera Condominium', '2020-04-12 23:41:31', '2020-04-12 23:41:31', NULL, NULL),
(2, 'Christine G Herda', 'sabrinna@gmail.com', '123', 'Inquiring for Plumera Condominium', '2020-04-12 23:43:46', '2020-04-12 23:43:46', NULL, NULL),
(3, 'Christine G Herda', 'sabrinna@gmail.com', '123', 'Inquiring for Plumera Condominium', '2020-04-12 23:50:09', '2020-04-12 23:50:09', NULL, NULL),
(4, 'Christine G Herda', 'sabrinna@gmail.com', '123', 'Inquiring for Plumera Condominium', '2020-04-12 23:50:22', '2020-04-12 23:50:22', NULL, NULL),
(5, 'Christine G Herda', 'sabrinna@gmail.com', '123', 'Inquiring for Plumera Condominium', '2020-04-12 23:50:50', '2020-04-12 23:50:50', NULL, NULL),
(6, 'Scarlett Eve Herad', 'scarlett@gmail.com', '123', 'Contact Us test', '2020-04-12 23:52:47', '2020-04-12 23:52:47', NULL, NULL),
(7, 'Scarlett Eve Herda', 'sabrinna@gmail.com', '123', 'Inquiry for Plumera', '2020-04-12 23:55:35', '2020-04-12 23:55:35', NULL, NULL),
(8, 'Christine G Herda', 'christine.gonx@gmail.com', '09062721161', 'Please send computation.', '2020-04-13 03:07:06', '2020-04-13 03:07:06', NULL, NULL),
(9, 'Christin', 'christine.gonx@gmail.com', '09062721161', 'How much is the monthly equity?', '2020-05-05 05:03:50', '2020-05-05 05:03:50', NULL, NULL),
(10, 'Scar', 'scar@gmail.com', '123', 'test', '2020-05-05 05:17:45', '2020-05-05 05:17:45', NULL, NULL),
(11, 'Scar', 'scar@gmail.com', '123', 'test', '2020-05-05 05:19:54', '2020-05-05 05:19:54', NULL, NULL),
(12, 'sab', 'sab@gmail.com', '123', 'test', '2020-05-05 05:20:25', '2020-05-05 05:20:25', NULL, NULL),
(13, 'John', 'john@gmail.com', '123', 'test', '2020-05-05 05:22:21', '2020-05-05 05:22:21', NULL, NULL),
(14, 'sab', 'sab@gmail.com', '123', 'testing', '2020-05-05 05:22:57', '2020-05-05 05:22:57', NULL, NULL),
(15, 'sab', 'sab@gmail.com', '123', 'testing', '2020-05-05 05:23:10', '2020-05-05 05:23:10', NULL, NULL),
(16, 'sab', 'sab@gmail.com', '123', 'testing', '2020-05-05 05:23:51', '2020-05-05 05:23:51', NULL, NULL),
(17, 'Sam', 'sam@gmail.com', '123', 'hello message', '2020-05-05 05:25:11', '2020-05-05 05:25:11', NULL, NULL),
(18, 'Hello Hi', 'hello@gmail.com', '123', 'Id like to visit', '2020-05-05 05:26:06', '2020-05-05 05:26:06', NULL, NULL),
(19, 'Christine G Herda', 'scar@gmail.com', '123', 'Testing', '2020-05-25 02:55:39', '2020-05-25 02:55:39', 'cebu', 'House & Lot'),
(20, 'Scarlett', 'scar@gmail.com', '123', 'testing1', '2020-05-25 02:59:05', '2020-05-25 02:59:05', 'St Francis Hills (Single Attached)', 'House & Lot');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, '{}', 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
(48, 6, 'body', 'rich_text_box', 'Body', 0, 0, 1, 1, 1, 1, '{}', 6),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 7),
(50, 6, 'meta_description', 'text', 'Meta Description', 0, 0, 1, 1, 1, 1, '{}', 8),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 0, 0, 1, 1, 1, 1, '{}', 9),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 10),
(53, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 11),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, '{}', 5),
(56, 6, 'video_link', 'text', 'Video Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(57, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(59, 7, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 6),
(60, 7, 'address', 'text', 'Address', 0, 1, 1, 1, 1, 1, '{}', 4),
(61, 7, 'bedrooms', 'number', 'Bedrooms', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 7),
(62, 7, 'toilet_bath', 'number', 'Toilet Bath', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 8),
(64, 7, 'lot_area', 'text', 'Lot Area', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 10),
(65, 7, 'floor_area', 'text', 'Floor Area', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 11),
(66, 7, 'status', 'select_dropdown', 'Status', 0, 0, 1, 1, 1, 1, '{\"default\":\"ACTIVE\",\"options\":{\"ACTIVE\":\"ACTIVE\",\"SOLD\":\"SOLD\"}}', 16),
(84, 7, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:properties,slug\"},\"display\":{\"width\":\"6\"}}', 3),
(85, 7, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 23),
(86, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 24),
(102, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(103, 8, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(104, 8, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true}}', 4),
(105, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(106, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(107, 8, 'brand', 'image', 'Brand', 0, 1, 1, 1, 1, 1, '{}', 3),
(109, 7, 'developer', 'select_dropdown', 'Developer', 0, 0, 1, 1, 1, 1, '{\"default\":\"Priland\",\"options\":{\"Priland\":\"Priland\",\"RTU Land\":\"RTU Land\",\"Castille\":\"Castille\",\"Nexus\":\"Nexus\",\"Greentech\":\"Greentech\",\"ABC Prime\":\"ABC Prime\",\"Cebu Land Master\":\"Cebu Land Master\",\"Vanderbuilt\":\"Vanderbuilt\",\"Golden Topper\":\"Golden Topper\",\"Aldev Corporation\":\"Aldev Corporation\",\"Saekyung\":\"Saekyung\",\"Taft Properties\":\"Taft Properties\",\"Johndorf Venture Coporation\":\"Johndorf Venture Coporation\"}}', 14),
(112, 7, 'price', 'text', 'Price', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 12),
(113, 7, 'monthly', 'text', 'Monthly', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 13),
(115, 7, 'featured', 'checkbox', 'Featured in Home', 0, 1, 1, 1, 1, 1, '{}', 17),
(116, 7, 'property_type', 'select_dropdown', 'Property Type', 0, 0, 1, 1, 1, 1, '{\"default\":\"House & Lot\",\"options\":{\"House & Lot\":\"House & Lot\",\"Condominium\":\"Condominium\"}}', 19),
(118, 7, 'model_type', 'text', 'Model Type', 0, 0, 1, 1, 1, 1, '{}', 21),
(119, 7, 'rfo', 'checkbox', 'RFO', 0, 1, 1, 1, 1, 1, '{}', 22),
(120, 7, 'type', 'select_dropdown', 'Type', 0, 0, 1, 1, 1, 1, '{\"default\":\"Townhouse\",\"options\":{\"Townhouse\":\"Townhouse\",\"Single Dettached\":\"Single Dettached\",\"Single Attached\":\"Single Attached\",\"Duplex\":\"Duplex\",\"Studio\":\"Studio\"}}', 20),
(121, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(122, 10, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(123, 10, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 3),
(124, 10, 'phone', 'text', 'Phone', 0, 1, 1, 1, 1, 1, '{}', 4),
(125, 10, 'message', 'text', 'Message', 0, 1, 1, 1, 1, 1, '{}', 5),
(126, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(127, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(128, 7, 'city', 'select_dropdown', 'City', 0, 0, 1, 1, 1, 1, '{\"default\":\"cebu\",\"options\":{\"cebu\":\"Cebu\",\"mandaue\":\"Mandaue\",\"liloan\":\"Liloan\",\"consolacion\":\"Consolacion\",\"Talisay\":\"Talisay\",\"lapu-lapu\":\"Lapu Lapu\"}}', 5),
(129, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(130, 11, 'city', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(131, 11, 'zipcode', 'text', 'Zipcode', 0, 0, 1, 1, 1, 1, '{}', 3),
(132, 11, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"city\",\"forceUpdate\":true}}', 4),
(133, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 5),
(134, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(142, 11, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 7),
(143, 11, 'featured', 'checkbox', 'Featured', 0, 0, 1, 1, 1, 1, '{}', 8),
(160, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 2),
(169, 7, 'parking', 'text', 'Parking', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 9),
(170, 7, 'body', 'rich_text_box', 'Body', 0, 0, 1, 1, 1, 1, '{}', 15),
(171, 7, 'vtour', 'text', 'Virtual Tour Link', 0, 0, 1, 1, 1, 1, '{}', 18),
(172, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(173, 14, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(174, 14, 'client_name', 'text', 'Client Name', 0, 1, 1, 1, 1, 1, '{}', 4),
(175, 14, 'city', 'text', 'City', 0, 0, 1, 1, 1, 1, '{}', 5),
(176, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 6),
(177, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(178, 14, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(179, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(180, 15, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(181, 15, 'link', 'text', 'Link', 0, 0, 1, 1, 1, 1, '{}', 4),
(182, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(183, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(184, 15, 'thumbnail', 'image', 'Thumbnail', 1, 0, 1, 1, 1, 1, '{}', 3),
(185, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(186, 16, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(187, 16, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(188, 16, 'body', 'text_area', 'Body', 0, 1, 1, 1, 1, 1, '{}', 4),
(189, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(190, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(191, 7, 'image_1', 'image', 'Image 1', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 25),
(192, 7, 'image_2', 'image', 'Image 2', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 26),
(193, 7, 'image_3', 'image', 'Image 3', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 27),
(194, 7, 'image_4', 'image', 'Image 4', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 28),
(195, 7, 'image_5', 'image', 'Image 5', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 29),
(196, 7, 'image_6', 'image', 'Image 6', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 30),
(197, 7, 'image_7', 'image', 'Image 7', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 31),
(198, 7, 'image_8', 'image', 'Image 8', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 32),
(199, 7, 'image_9', 'image', 'Image 9', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 33),
(200, 7, 'image_10', 'image', 'Image 10', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 34),
(201, 7, 'image_11', 'image', 'Image 11', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 35),
(202, 7, 'image_12', 'image', 'Image 12', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 36),
(203, 10, 'location', 'text', 'Location', 0, 1, 1, 1, 1, 1, '{}', 8),
(204, 10, 'property_type', 'text', 'Property Type', 0, 1, 1, 1, 1, 1, '{}', 9),
(205, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(206, 18, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(207, 18, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(208, 18, 'subtitle', 'text', 'Subtitle', 0, 0, 1, 1, 1, 1, '{}', 4),
(209, 18, 'order', 'text', 'Order', 0, 1, 1, 1, 1, 1, '{}', 5),
(210, 18, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 6),
(211, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-04-07 00:55:14', '2020-04-07 02:50:36'),
(7, 'properties', 'properties', 'Property', 'Properties', NULL, 'App\\Property', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-07 03:13:57', '2020-05-11 09:01:58'),
(8, 'developers', 'awards', 'Award', 'Awards', NULL, 'App\\Developer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-08 03:27:10', '2020-05-25 00:16:45'),
(10, 'contacts', 'contacts', 'Contact', 'Contacts', NULL, 'App\\Contact', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-11 01:53:31', '2020-05-25 02:55:11'),
(11, 'cities', 'cities', 'City', 'Cities', NULL, 'App\\City', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-13 00:03:46', '2020-05-04 01:24:13'),
(14, 'sold_properties', 'sold-properties', 'Sold Property', 'Sold Properties', NULL, 'App\\SoldProperty', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-05-04 20:42:08', '2020-05-04 20:44:25'),
(15, 'videos', 'videos', 'Video', 'Videos', NULL, 'App\\Video', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-05-04 23:18:40', '2020-05-04 23:35:01'),
(16, 'testimonials', 'testimonials', 'Testimonial', 'Testimonials', NULL, 'App\\Testimonial', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-05-05 00:02:53', '2020-05-05 00:02:53'),
(18, 'banners', 'banners', 'Banner', 'Banners', NULL, 'App\\Banner', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-05-26 01:49:32', '2020-05-26 01:49:41');

-- --------------------------------------------------------

--
-- Table structure for table `developers`
--

CREATE TABLE `developers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(265) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(265) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `brand` varchar(265) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `developers`
--

INSERT INTO `developers` (`id`, `name`, `slug`, `created_at`, `updated_at`, `brand`) VALUES
(1, 'Top 2 On our Semi Annual Awarding', 'top-2-on-our-semi-annual-awarding', '2020-04-08 03:31:06', '2020-05-25 00:33:04', 'awards/May2020/b4kFA7RlKHidS2YwIcgO.jpg'),
(2, 'Top 3 in Priland Developer', 'top-3-in-priland-developer', '2020-04-08 04:25:20', '2020-05-25 00:33:55', 'awards/May2020/vQ4nvIKRj0sbjzp0U2Rb.jpg'),
(3, 'Top 2 Awardee from ASR Realty', 'top-2-awardee-from-asr-realty', '2020-04-10 22:06:58', '2020-05-25 00:35:11', 'awards/May2020/YQj6vQeCIOF0vbyniyFD.jpg'),
(4, 'Silver Awardee 2019 from ASR Realty', 'silver-awardee-2019-from-asr-realty', '2020-04-10 22:07:16', '2020-05-25 00:36:16', 'awards/May2020/PetyVM803C8QhZzj1z42.jpg'),
(5, 'Certificate of Recognition from Durosland Property', 'certificate-of-recognition-from-durosland-property', '2020-05-25 00:36:51', '2020-05-25 00:36:51', 'awards/May2020/XHKyR3DyLzsExgWxSK9Z.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-04-07 00:31:41', '2020-05-09 23:35:55'),
(2, 'header', '2020-04-09 00:16:49', '2020-04-09 00:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-04-07 00:31:41', '2020-04-07 00:31:41', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2020-04-07 00:31:41', '2020-04-09 00:24:00', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-04-07 00:31:41', '2020-04-07 00:31:41', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-04-07 00:31:41', '2020-04-07 00:31:41', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 10, '2020-04-07 00:31:41', '2020-05-25 00:23:50', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-04-07 00:31:41', '2020-04-09 00:24:00', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-04-07 00:31:41', '2020-04-09 00:24:00', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-04-07 00:31:41', '2020-04-09 00:24:00', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-04-07 00:31:41', '2020-04-09 00:24:00', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 11, '2020-04-07 00:31:41', '2020-05-25 00:23:50', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-04-07 00:31:41', '2020-04-09 00:24:00', 'voyager.hooks', NULL),
(13, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 5, '2020-04-07 00:55:14', '2020-04-09 00:24:00', 'voyager.posts.index', NULL),
(14, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2020-04-07 00:55:14', '2020-04-09 00:24:00', 'voyager.pages.index', NULL),
(15, 1, 'Property', '', '_self', 'voyager-company', '#000000', 17, 1, '2020-04-07 03:13:57', '2020-04-09 00:24:02', 'voyager.properties.index', 'null'),
(16, 1, 'Awards', '', '_self', 'voyager-certificate', '#000000', 35, 1, '2020-04-08 03:27:10', '2020-05-25 00:21:14', 'voyager.awards.index', 'null'),
(17, 1, 'Properties', '', '_self', 'voyager-shop', '#000000', NULL, 8, '2020-04-09 00:23:29', '2020-04-09 00:24:00', NULL, ''),
(18, 2, 'About Us', '/about', '_self', NULL, '#000000', NULL, 5, '2020-04-11 00:23:30', '2020-05-10 21:59:40', NULL, ''),
(19, 2, 'House & Lot', '/house-and-lot', '_self', NULL, '#000000', NULL, 2, '2020-04-11 00:23:51', '2020-05-10 21:59:40', NULL, ''),
(20, 2, 'Condominium', '/condominium', '_self', NULL, '#000000', NULL, 3, '2020-04-11 00:24:01', '2020-05-10 21:59:40', NULL, ''),
(21, 2, 'RFO', '/rfo', '_self', NULL, '#000000', NULL, 4, '2020-04-11 00:24:16', '2020-05-10 21:59:40', NULL, ''),
(22, 2, 'Contact Us', '/contact', '_self', NULL, '#000000', NULL, 7, '2020-04-11 00:24:27', '2020-05-10 21:59:40', NULL, ''),
(23, 1, 'Contacts', '', '_self', 'voyager-paper-plane', '#000000', NULL, 7, '2020-04-11 01:53:32', '2020-05-25 00:23:50', 'voyager.contacts.index', 'null'),
(24, 1, 'Cities', '', '_self', 'voyager-location', '#000000', 17, 3, '2020-04-13 00:03:46', '2020-05-25 00:26:23', 'voyager.cities.index', 'null'),
(25, 2, 'House and Lot', '/rfo/house-and-lot', '_self', NULL, '#000000', 21, 1, '2020-04-13 03:38:01', '2020-04-13 03:38:04', NULL, ''),
(26, 2, 'Condominium', '/rfo/condominium', '_self', NULL, '#000000', 21, 2, '2020-04-13 03:38:19', '2020-04-13 03:38:23', NULL, ''),
(28, 2, 'Videos', '/videos', '_self', NULL, '#000000', NULL, 6, '2020-04-27 06:58:21', '2020-05-10 21:59:40', NULL, ''),
(30, 1, 'Sold Properties', '', '_self', 'voyager-star', '#000000', 17, 2, '2020-05-04 20:42:08', '2020-05-25 00:26:00', 'voyager.sold-properties.index', 'null'),
(31, 1, 'Videos', '', '_self', 'voyager-video', '#000000', 35, 3, '2020-05-04 23:18:40', '2020-05-25 00:25:20', 'voyager.videos.index', 'null'),
(32, 1, 'Testimonials', '', '_self', 'voyager-bubble-hear', '#000000', 35, 2, '2020-05-05 00:02:53', '2020-05-25 00:25:07', 'voyager.testimonials.index', 'null'),
(33, 2, 'Home', '/', '_self', NULL, '#000000', NULL, 1, '2020-05-10 21:59:33', '2020-05-10 21:59:40', NULL, ''),
(35, 1, 'Components', '', '_self', 'voyager-pie-chart', '#000000', NULL, 9, '2020-05-25 00:19:10', '2020-05-25 00:23:50', NULL, ''),
(36, 1, 'Banners', '', '_self', NULL, NULL, NULL, 12, '2020-05-26 01:49:32', '2020-05-26 01:49:32', 'voyager.banners.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_01_01_000000_add_voyager_user_fields', 1),
(3, '2016_01_01_000000_create_data_types_table', 1),
(4, '2016_05_19_173453_create_menu_table', 1),
(5, '2016_10_21_190000_create_roles_table', 1),
(6, '2016_10_21_190000_create_settings_table', 1),
(7, '2016_11_30_135954_create_permission_table', 1),
(8, '2016_11_30_141208_create_permission_role_table', 1),
(9, '2016_12_26_201236_data_types__add__server_side', 1),
(10, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(11, '2017_01_14_005015_create_translations_table', 1),
(12, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(13, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(14, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(15, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(16, '2017_08_05_000000_add_group_to_settings_table', 1),
(17, '2017_11_26_013050_add_user_role_relationship', 1),
(18, '2017_11_26_015000_create_user_roles_table', 1),
(19, '2018_03_11_000000_add_user_settings', 1),
(20, '2018_03_14_000000_add_details_to_data_types_table', 1),
(21, '2018_03_16_000000_make_settings_value_nullable', 1),
(22, '2019_08_19_000000_create_failed_jobs_table', 1),
(23, '2016_01_01_000000_create_pages_table', 2),
(24, '2016_01_01_000000_create_posts_table', 2),
(25, '2016_02_15_204651_create_categories_table', 2),
(26, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `video_link` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`, `video_link`) VALUES
(1, 1, 'Home', NULL, NULL, 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2020-04-07 00:55:14', '2020-05-24 22:48:16', 'http://127.0.0.1:8000/images/Breeza.mp4'),
(2, 1, 'About', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'pages/April2020/5JsgPrMbjH3f7f8LgrCK.jpg', 'about', NULL, NULL, 'ACTIVE', '2020-04-10 02:58:35', '2020-04-11 00:50:37', NULL),
(3, 1, 'House & Lot', NULL, 'pages\\May2020\\WKFmW8eyrrhPaMbRR2VP.jpg', 'house-and-lot', NULL, NULL, 'ACTIVE', '2020-04-10 02:59:07', '2020-05-10 04:27:27', NULL),
(4, 1, 'Condominium', NULL, 'pages/April2020/5wbJZJ0HRvT1s91Wz0t0.jpg', 'condominium', NULL, NULL, 'INACTIVE', '2020-04-10 02:59:19', '2020-04-11 01:11:41', NULL),
(5, 1, 'RFO', NULL, 'pages/April2020/iq5dyW7r2N7QehMXjSVI.jpg', 'rfo', NULL, NULL, 'ACTIVE', '2020-04-10 02:59:30', '2020-04-11 01:18:32', NULL),
(6, 1, 'Pre-Selling', NULL, NULL, 'pre-selling', NULL, NULL, 'ACTIVE', '2020-04-10 02:59:45', '2020-04-10 02:59:45', NULL),
(7, 1, 'Contact Us', NULL, 'pages/May2020/pW1Sb0lHeaULOn7B7PzJ.jpg', 'contact-us', NULL, NULL, 'ACTIVE', '2020-04-10 02:59:57', '2020-05-25 01:10:54', NULL),
(8, 1, 'Videos', NULL, 'pages/May2020/0xN9LdSa0EmW16rZJjWi.jpg', 'videos', NULL, NULL, 'ACTIVE', '2020-04-27 07:00:04', '2020-05-25 01:15:29', NULL),
(9, 1, 'Search Results', NULL, 'pages/May2020/3DIEzyGmCzKPUl0g2Ye0.jpg', 'search-results', NULL, NULL, 'ACTIVE', '2020-05-05 04:48:54', '2020-05-05 04:48:54', NULL),
(10, 1, 'Site Credits', '<p>This website was deployed and is managed by Christine G Herda. For inquiries, you may email at <a href=\"mailto:christine.gonx@gmail.com\">christine.gonx@gmail.com</a></p>', 'pages/May2020/LsmMmKHfa9De8BoZfBd4.jpg', 'site-credits', NULL, NULL, 'INACTIVE', '2020-05-25 01:07:01', '2020-05-25 01:10:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(2, 'browse_bread', NULL, '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(3, 'browse_database', NULL, '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(4, 'browse_media', NULL, '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(5, 'browse_compass', NULL, '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(6, 'browse_menus', 'menus', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(7, 'read_menus', 'menus', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(8, 'edit_menus', 'menus', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(9, 'add_menus', 'menus', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(10, 'delete_menus', 'menus', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(11, 'browse_roles', 'roles', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(12, 'read_roles', 'roles', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(13, 'edit_roles', 'roles', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(14, 'add_roles', 'roles', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(15, 'delete_roles', 'roles', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(16, 'browse_users', 'users', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(17, 'read_users', 'users', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(18, 'edit_users', 'users', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(19, 'add_users', 'users', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(20, 'delete_users', 'users', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(21, 'browse_settings', 'settings', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(22, 'read_settings', 'settings', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(23, 'edit_settings', 'settings', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(24, 'add_settings', 'settings', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(25, 'delete_settings', 'settings', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(26, 'browse_hooks', NULL, '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(27, 'browse_categories', 'categories', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(28, 'read_categories', 'categories', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(29, 'edit_categories', 'categories', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(30, 'add_categories', 'categories', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(31, 'delete_categories', 'categories', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(32, 'browse_posts', 'posts', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(33, 'read_posts', 'posts', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(34, 'edit_posts', 'posts', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(35, 'add_posts', 'posts', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(36, 'delete_posts', 'posts', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(37, 'browse_pages', 'pages', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(38, 'read_pages', 'pages', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(39, 'edit_pages', 'pages', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(40, 'add_pages', 'pages', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(41, 'delete_pages', 'pages', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(42, 'browse_properties', 'properties', '2020-04-07 03:13:57', '2020-04-07 03:13:57'),
(43, 'read_properties', 'properties', '2020-04-07 03:13:57', '2020-04-07 03:13:57'),
(44, 'edit_properties', 'properties', '2020-04-07 03:13:57', '2020-04-07 03:13:57'),
(45, 'add_properties', 'properties', '2020-04-07 03:13:57', '2020-04-07 03:13:57'),
(46, 'delete_properties', 'properties', '2020-04-07 03:13:57', '2020-04-07 03:13:57'),
(47, 'browse_developers', 'developers', '2020-04-08 03:27:10', '2020-04-08 03:27:10'),
(48, 'read_developers', 'developers', '2020-04-08 03:27:10', '2020-04-08 03:27:10'),
(49, 'edit_developers', 'developers', '2020-04-08 03:27:10', '2020-04-08 03:27:10'),
(50, 'add_developers', 'developers', '2020-04-08 03:27:10', '2020-04-08 03:27:10'),
(51, 'delete_developers', 'developers', '2020-04-08 03:27:10', '2020-04-08 03:27:10'),
(52, 'browse_contacts', 'contacts', '2020-04-11 01:53:32', '2020-04-11 01:53:32'),
(53, 'read_contacts', 'contacts', '2020-04-11 01:53:32', '2020-04-11 01:53:32'),
(54, 'edit_contacts', 'contacts', '2020-04-11 01:53:32', '2020-04-11 01:53:32'),
(55, 'add_contacts', 'contacts', '2020-04-11 01:53:32', '2020-04-11 01:53:32'),
(56, 'delete_contacts', 'contacts', '2020-04-11 01:53:32', '2020-04-11 01:53:32'),
(57, 'browse_cities', 'cities', '2020-04-13 00:03:46', '2020-04-13 00:03:46'),
(58, 'read_cities', 'cities', '2020-04-13 00:03:46', '2020-04-13 00:03:46'),
(59, 'edit_cities', 'cities', '2020-04-13 00:03:46', '2020-04-13 00:03:46'),
(60, 'add_cities', 'cities', '2020-04-13 00:03:46', '2020-04-13 00:03:46'),
(61, 'delete_cities', 'cities', '2020-04-13 00:03:46', '2020-04-13 00:03:46'),
(72, 'browse_sold_properties', 'sold_properties', '2020-05-04 20:42:08', '2020-05-04 20:42:08'),
(73, 'read_sold_properties', 'sold_properties', '2020-05-04 20:42:08', '2020-05-04 20:42:08'),
(74, 'edit_sold_properties', 'sold_properties', '2020-05-04 20:42:08', '2020-05-04 20:42:08'),
(75, 'add_sold_properties', 'sold_properties', '2020-05-04 20:42:08', '2020-05-04 20:42:08'),
(76, 'delete_sold_properties', 'sold_properties', '2020-05-04 20:42:08', '2020-05-04 20:42:08'),
(77, 'browse_videos', 'videos', '2020-05-04 23:18:40', '2020-05-04 23:18:40'),
(78, 'read_videos', 'videos', '2020-05-04 23:18:40', '2020-05-04 23:18:40'),
(79, 'edit_videos', 'videos', '2020-05-04 23:18:40', '2020-05-04 23:18:40'),
(80, 'add_videos', 'videos', '2020-05-04 23:18:40', '2020-05-04 23:18:40'),
(81, 'delete_videos', 'videos', '2020-05-04 23:18:40', '2020-05-04 23:18:40'),
(82, 'browse_testimonials', 'testimonials', '2020-05-05 00:02:53', '2020-05-05 00:02:53'),
(83, 'read_testimonials', 'testimonials', '2020-05-05 00:02:53', '2020-05-05 00:02:53'),
(84, 'edit_testimonials', 'testimonials', '2020-05-05 00:02:53', '2020-05-05 00:02:53'),
(85, 'add_testimonials', 'testimonials', '2020-05-05 00:02:53', '2020-05-05 00:02:53'),
(86, 'delete_testimonials', 'testimonials', '2020-05-05 00:02:53', '2020-05-05 00:02:53'),
(87, 'browse_banners', 'banners', '2020-05-26 01:49:32', '2020-05-26 01:49:32'),
(88, 'read_banners', 'banners', '2020-05-26 01:49:32', '2020-05-26 01:49:32'),
(89, 'edit_banners', 'banners', '2020-05-26 01:49:32', '2020-05-26 01:49:32'),
(90, 'add_banners', 'banners', '2020-05-26 01:49:32', '2020-05-26 01:49:32'),
(91, 'delete_banners', 'banners', '2020-05-26 01:49:32', '2020-05-26 01:49:32');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Location', NULL, 'Ask yourself the following questions:\r\n\r\n- Is it accessible to transportation?\r\n- How close is it to work, schools, hospitals and markets?\r\n- Is the neighborhood safe?\r\n- Community profile?', '<p>Ask yourself the following questions:<br /><br />- Is it accessible to transportation?<br />- How close is it to work, schools, hospitals and markets?<br />- Is the neighborhood safe?<br />- Community profile?</p>', 'posts/May2020/85ODhnU3U7UsCCHGfEyv.jpg', 'location', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 1, '2020-04-07 00:55:14', '2020-05-25 02:40:04'),
(2, 1, 1, 'Budget', NULL, 'According to Pag-Ibig HMDF, the amount you should allot for your housing loan should not be more than 40% of you monthly income.\r\n\r\nSo if your gross household (married couple) income is P50,000 every month, you ideally can afford up to P20,000  monthly for amortization.', '<p>According to Pag-Ibig HMDF, the amount you should allot for your housing loan should not be more than 40% of you monthly income.<br /><br />So if your gross household (married couple) income is P50,000 every month, you ideally can afford up to P20,000&nbsp; monthly for amortization.</p>', 'posts/May2020/VBrRgBxsfMgHzi2Cjkkk.jpeg', 'budget', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 1, '2020-04-07 00:55:14', '2020-05-25 02:39:46'),
(3, 1, 1, 'Timeline', NULL, 'How soon do you need a house?\r\n\r\nThe earlier you invest the better for you. Prices of real estate generally increase by 30% - 40% every year. \r\n\r\nPag-ibig HMDF also has age requirements on housing loan applications.', '<p>How soon do you need a house?<br /><br />The earlier you invest the better for you. Prices of real estate generally increase by 30% - 40% every year.&nbsp;<br /><br />Pag-ibig HMDF also has age requirements on housing loan applications.</p>', 'posts/May2020/GBPpVpnksCbyWabcU2Tk.jpg', 'timeline', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 1, '2020-04-07 00:55:14', '2020-05-25 02:39:31'),
(4, 1, 1, 'Choose the Realtor', NULL, 'Who:\r\n- Has Market Knowledge\r\n- Offers Options\r\n- Partners with you in finding the best real estate fit!', '<p>Who:<br />- Has Market Knowledge<br />- Offers Options<br />- Partners with you in finding the best real estate fit!</p>', 'posts/May2020/967Ij7IOEKD8PYJF4gqk.jpg', 'choose-the-realtor', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 1, '2020-04-07 00:55:14', '2020-05-25 02:39:04'),
(5, 1, 1, 'The Developer', NULL, '- Ask & check quality of turned over projects.\r\n- Check for credibility\r\n- Check HLURB LTS or License to Sell', '<p>- Ask &amp; check quality of turned over projects.<br />- Check for credibility<br />- Check HLURB LTS or License to Sell</p>', 'posts\\May2020\\kIGdVxYFGA42i5vWJMP2.jpg', 'the-developer', NULL, NULL, 'PUBLISHED', 0, '2020-05-11 23:59:36', '2020-05-11 23:59:36');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(265) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `bedrooms` int(11) DEFAULT NULL,
  `toilet_bath` int(11) DEFAULT NULL,
  `parking` int(11) DEFAULT NULL,
  `lot_area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `floor_area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `developer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfo` int(11) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vtour` text COLLATE utf8mb4_unicode_ci,
  `image_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_11` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_12` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `title`, `image`, `address`, `bedrooms`, `toilet_bath`, `parking`, `lot_area`, `floor_area`, `status`, `body`, `slug`, `created_at`, `updated_at`, `developer`, `property_type`, `price`, `monthly`, `featured`, `type`, `model_type`, `rfo`, `city`, `vtour`, `image_1`, `image_2`, `image_3`, `image_4`, `image_5`, `image_6`, `image_7`, `image_8`, `image_9`, `image_10`, `image_11`, `image_12`, `order`) VALUES
(6, 'Antara Condominium', 'properties\\May2020\\lnl4U9VX6tpBzYrxTQtS.jpeg', 'Talisay, Cebu', NULL, NULL, NULL, '64 SQM', '58 SQM', 'ACTIVE', '<p>AVAILABLE:<br />&bull; Studio Type<br />&bull; 1 Bedroom<br />&bull; 2 Bedrooms<br /><br />AMENITIES:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Retail Strip<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Clubhouse With Infinity And Kiddie Pool<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Walking/jogging Trail<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Mini Golf Course<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Zipline And Wall Climbing<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Picnic Huts<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Meditation Pavillion<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Organic Garden Plots<br /><br />CONDOMINIUM DETAILS:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>With Balcony<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Luxe Plank Flooring<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Painted Walls, Partitions And Ceiling<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Telephone / Cable Provisions<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Toilet &amp; Bath With Fixtures, Tiles &amp; Exhaust Fan<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Kitchen Sink With Grease Strap<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Laminated Countertop &amp; Cabinets<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Washing Machine Area Provision<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Smoke Detector &amp; Water Sprinklers<br /><br />Landmarks/Accessibility:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>5 minutes from subdivision to highway<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>5 minutes going to Star Mall and Jollibee Minglanilla<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>8 minutes going to Minglanilla District Hospital<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>10 minutes going to Aqua cainta Resort<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>10 minutes going to Ct. Cecilia College</p>', 'antara-condominium', '2020-05-04 05:23:00', '2020-05-10 21:04:02', 'Castille', 'Condominium', '3,069,074', '16,832', 0, 'Studio', NULL, 0, 'Talisay', 'https://www.youtube.com/channel/UCod-sYLoUMEHU7nHuDReiUA', 'properties\\May2020\\Ce4N153LJRZhaoKY8Uvw.jpg', 'properties\\May2020\\TDHtR1UqxYtronA83uEz.jpg', 'properties\\May2020\\TTCKV82sAbobZUQQJv9x.jpg', 'properties\\May2020\\ccfi6KUiGP6KZr8HUf22.jpg', 'properties\\May2020\\i4a9SJ6FuOxue1ONZyWa.jpg', 'properties\\May2020\\bUmibH7IZOnl5U9DKxVj.png', 'properties\\May2020\\0DmIPAC1GxsVt8IlUreH.jpg', 'properties\\May2020\\U1vsy7b4Q6kIXxLRGxug.jpg', 'properties\\May2020\\VxL1iHXRVlqF0VXsR39i.jpg', 'properties\\May2020\\3t6Flsu6Q3YLDJH7c8n0.jpg', 'properties\\May2020\\P66MFC9R9fcGvvXROFSD.jpg', 'properties\\May2020\\iy4DU4spSkeLIoPHqH8L.jpg', 0),
(7, 'St Francis Hills (Single Attached)', 'properties\\May2020\\npwlgXCASYuYTWwRQnVS.jpg', 'Tolo-Tolo, Consolacion', 2, 2, 1, '64 SQM', '58 SQM', 'ACTIVE', '<p>HOUSE DETAILS<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>2 Bedrooms (Expandable to 3)<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>2 Toilet And Bath<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Living And Dining Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Kitchen And Service Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Carpark<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Porch<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Floor Area: 58sqm<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lot Area: 64sqm<br /><br />AMENITIES:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Landscaped Entrance Gate<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Multi-purpose Hall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Chapel<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Children&rsquo;s Playground<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Jogging Trails<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Shuttle Service Vehicles<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Complete Facilities For Drainage Power Supply<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Water Supply<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Telephone<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lighted Tree-lined Concrete Roads<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Shuttle Service Vehicles<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Perimeter Fence<br /><br />Landmarks/Accessibility:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>5 minutes from Fooda, Consolacion Church and Main Road<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 6</span> minutes going to City Mall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 7</span> minutes going to SM Consolacion<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 8</span> minutes going to Mendero Medical Center</p>', 'siena-st-francis-hills', '2020-05-10 01:15:00', '2020-05-10 09:38:05', 'Castille', 'House & Lot', '3,069,074', '16,832', 1, 'Single Attached', 'Siena', 0, 'consolacion', NULL, 'properties\\May2020\\MmSCJt126LBRRwVlhUQy.jpg', 'properties\\May2020\\gW27cgYmPSMk6dyFsRC2.jpg', 'properties\\May2020\\3PfcbYeEg9C2RK4vG3PU.jpg', 'properties\\May2020\\epJzqdzMJWKT8XsfGWuX.jpg', 'properties\\May2020\\aDcG0TBbnkRJ0FU1KOKM.jpg', 'properties\\May2020\\63vth5spD00T23gzL3TT.jpg', 'properties\\May2020\\2tx2segQXmFKRbmMkY4A.jpg', 'properties\\May2020\\YenCnN3vBbmlP8NbUjDd.jpg', 'properties\\May2020\\fffaGKIWIv3i1iXAbaVi.jpg', 'properties\\May2020\\mMEeHeppE5WFMRPV8rNC.jpg', 'properties\\May2020\\RPplLiOHStfSHtXElOE8.jpg', 'properties\\May2020\\1YCLopBRj2hHHJZ4kJ27.jpg', 0),
(8, 'Belize North (Single Detached)', 'properties\\May2020\\QTu6jVurDfXnsOnWLxwo.jpg', 'Nangka, Consolacion, Cebu', 5, 3, 2, '130 SQM', '157 SQM', 'ACTIVE', '<p>HOUSE DETAILS<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>5 Bedrooms<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>3 Toilet &amp; Bath<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Living And Dining Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Kitchen And Service Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Carpark<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Floor Area : 157 sqm<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lot Area : 130 sqm<br /><br />AMENITIES:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Ample Guest Parking<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Basketball Court<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Clubhouse<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Children&rsquo;s Playground<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Jogging Trails<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Landscaped Gardens And Hardscapes<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Street Lights<br /><br /><br />Landmarks/Accessibility:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 4</span>&nbsp;minutes from Fooda, Consolacion Church and Main Road<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 5</span>&nbsp;minutes going to City Mall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 6</span>&nbsp;minutes going to SM Consolacion<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 7</span>&nbsp;minutes going to Mendero Medical Center</p>', 'belize-north', '2020-05-10 02:15:00', '2020-05-11 07:16:26', 'Priland', 'House & Lot', '8,087,056', '39,624', 1, 'Single Dettached', 'Celeste', 0, 'consolacion', NULL, 'properties\\May2020\\NtiF5ML087p0RlN6Z9Uj.jpg', 'properties\\May2020\\5dEbV1u8EoMMMQfgX2Xx.jpg', 'properties\\May2020\\xzG7fvdfKuCMpIKLn6cH.jpg', 'properties\\May2020\\IctBwNDqXYlkOA07StRW.jpg', 'properties\\May2020\\wPEfYleq7krdVovLw0I2.png', 'properties\\May2020\\ECw2gJyRlM8GqPIfDO2L.jpg', 'properties\\May2020\\mELJqJP9RsGWrqReiPx3.jpg', 'properties\\May2020\\wGOoA4IZtWDfYXVzVxLe.jpg', 'properties\\May2020\\ACp5TQu6hhV6L7DtB0qG.jpg', NULL, NULL, NULL, 0),
(9, 'Scottsdale (Townhouse)', 'properties\\May2020\\8VpsbrUgfAMYwwFZKdip.jpg', 'M. L. Quezon Ave, Mandaue City', 4, 3, 1, '50 SQM', '83.91 SQM', 'ACTIVE', '<p>HOUSE DETAILS<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 3</span> Bedrooms<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 1 Maids Quarter</span><br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 3</span>&nbsp;Toilet And Bath<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Living And Dining Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Kitchen And Service Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Carpark<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Floor Area: 50 sqm<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lot Area: 83.91 sqm<br /><br />Landmarks/Accessibility:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 1</span> minute from Main Road<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 3</span>&nbsp;minutes going to Metro Supermarket A.S Fortuna&nbsp;<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">3</span>&nbsp;minutes going to Gaisano Casuntingan</p>', 'scottsdale', '2020-05-10 02:47:00', '2020-05-10 09:34:29', 'Priland', 'House & Lot', '4,520,000', '29,000', 0, 'Townhouse', NULL, 0, 'mandaue', NULL, 'properties\\May2020\\suz9YIKKvINCGEnLTOM3.jpg', 'properties\\May2020\\emfvh0OAj9JAz4AGmQQt.jpg', 'properties\\May2020\\chr0ecVAH9XySC1lJRMj.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, 'City Clou', 'properties\\May2020\\Tp5UWddf9gE5FT3q5hj6.jpg', 'Cebu, City', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>AVAILABLE:<br />&bull; Studio Type<br />&bull; 1 Bedroom<br />&bull; 2 Bedrooms<br /><br />AMENITIES:<br />&bull; 24 Hour Security<br />&bull; Residential Entrance<br />&bull; Drop Off Area<br />&bull; Clubhouse<br />&bull; Kiddie Pool<br />&bull; Lap Pool<br />&bull; Fun Pool<br />&bull; Meditation Garden<br />&bull; Kids Playground<br />&bull; Plaza<br />&bull; Fitness Gym<br />&bull; Commercial Component</p>\r\n<p>Landmarks/Accessibility:<br />&bull; Project site is located at 0549 Dionisio Jakosalem Street., Cebu City<br />&bull; Across Sacred Heart Parish<br />&bull; Walking distance from Velez College &amp; Mango Avenue)<br />&bull; 1.5km Cebu Business Park<br />&bull; 2.1km Robinsons Galleria<br />&bull; 4.5km SM Seaside<br />&bull; 2.8km Cebu IT Park<br />&bull; 14.3km Mactan International Airport</p>', 'city-clou', '2020-05-10 03:22:00', '2020-05-10 09:37:33', 'Golden Topper', 'Condominium', '3,199,000', '15,370', 1, 'Studio', NULL, 0, 'cebu', NULL, 'properties\\May2020\\HZqoEpcTBQ84m09Xff1e.jpg', 'properties\\May2020\\lWgJ3uQrEMmObTOFMYSZ.jpg', 'properties\\May2020\\P9rSJfsDYDMdWPB9tfq1.jpg', 'properties\\May2020\\hV2CO6MwhA706iBtEd7B.jpg', 'properties\\May2020\\yqlTeSgiXi4B4OslYkmv.jpg', 'properties\\May2020\\hhVPWjWcouxlnTSxEcG8.jpg', 'properties\\May2020\\MZZSx08yDPvpvUm6yezB.jpg', 'properties\\May2020\\WnTBGKlvuGnxV9aHmC3T.jpg', 'properties\\May2020\\0uqG1vkwtta96WEqvEzQ.jpg', 'properties\\May2020\\GFxx1r5nOn6pQQWFhu7e.jpg', 'properties\\May2020\\WZPlsclsts2SGPNDBkSB.jpg', 'properties\\May2020\\W2ZmRcw8R0XfgzHVMfa0.jpg', 0),
(11, 'Plumera Mactan', 'properties\\May2020\\PdmBjOsjHQIX7lYcZo6L.jpg', 'Basak, Kagudoy, Lapu-Lapu', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>PROJECT FEATURES:<br />&bull; Gate and Guardhouse<br />&bull; Perimeter Fence<br />&bull; 15-M Wide Main Spine Road<br />&bull; 12-M Wide Primary Road<br />&bull; Fire Protection System<br />&bull; Lobby and Reception Area<br />&bull; Mailbox Area<br />&bull; Material Recovery Facility<br />&bull; Underground Drainage and Sewer System<br /><br />PROJECT AMENITIES:&nbsp;<br />&bull; Clubhouse and Multi-Purpose Area<br />&bull; Swimming Pool<br />&bull; Multi-Purpose Court<br />&bull; Pocket Parks<br />&bull; Future Retail Area<br /><br />UNIT DELIVERABLE:<br />&bull; Kitchen Cabinet with Countertop<br />&bull; Toilet and Bath with Fixtures<br />&bull; Balcony And Drying Area<br /><br />Landmarks/Accessibility:<br />&bull; 5 Minutes Going Mactan Doctors Hospital<br />&bull; 7 Minutes Going Pueblo VERDE/Mepz11/Tamiya<br />&bull; Beside Indiana Aerospace University <br />&bull; 10 Minutes Going Mactan International Airport</p>\r\n<p>&nbsp;</p>', 'plumera-mactan', '2020-05-10 03:55:00', '2020-05-12 08:34:54', 'Johndorf Venture Coporation', 'Condominium', '1,515,240', '7,956', 0, 'Studio', NULL, 0, 'lapu-lapu', NULL, 'properties\\May2020\\6hh3hqVdT1qDkn2t6uEm.jpg', 'properties\\May2020\\01OJqhbbuveuqGRH8V8T.jpg', 'properties\\May2020\\oKuTJ2PB55A4W45atTgZ.jpg', 'properties\\May2020\\oB9th3by1tFYAxapzj45.jpg', 'properties\\May2020\\NQRzghAq91ON8CjjwTvI.jpg', 'properties\\May2020\\P4GvTbKVft4dSxf9tHyk.jpg', 'properties\\May2020\\mq1Py1GR7uahWRwAHLvw.jpg', 'properties\\May2020\\cT1PL39fh0ljwapaRxNN.jpg', 'properties\\May2020\\DLC16Ckb7yacH6c5iORV.jpg', 'properties\\May2020\\wNE6s9mJbtC7MzFU7AMM.png', NULL, NULL, 0),
(12, 'ARC Tower', 'properties\\May2020\\MO4z74VpCjfWnWkaaJmk.jpg', 'N. Bacalso Avenue, Corner V. Rama St.', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>AVAILABLE:<br />&bull; Studio Type Without Balcony - 22.20 SQM<br />&bull; Studio Type With Balcony - 25 SQM&nbsp;<br /><br />AMENITIES:<br />&bull; 4 Levels Of Podium Parking<br />&bull; 4 Hi Speed Elevators<br />&bull; Elevator Key Card Access<br />&bull; Fire Alarm System &amp; Sprinklers<br />&bull; 24- Hour Security System<br />&bull; Building Management System<br />&bull; Main Lobby With Reception Area<br />&bull; Individual Mail Boxes<br />&bull; Work Space And Study Lounge<br />&bull; Conference/function Room<br />&bull; Fitness Gym<br />&bull; 25 Meter Swimming Pool And Sundeck<br />&bull; Kiddie Pool<br />&bull; Sky Garden<br />&bull; Viewing Deck<br /><br />UNIT DELIVERABLE:<br />&bull; Porcelain Tiles In All Dwelling Areas<br />&bull; Painted Walls And Ceilings<br />&bull; Universal Type Convenience Outlet<br />&bull; Grease Trap<br />&bull; Smoke Detection System &amp; Sprinkler System<br />&bull; High Quality Toilet Bath Features And Accessories<br />&bull; Complete Lighting Fixtures<br />&bull; Kitchen Countertop<br />&bull; Upper And Lower Cabinets<br />&bull; Individual Electric And Water Meter<br />&bull; Provision For Cable Tv, Telephone And Internet Line<br />&bull; Provision For Kitchen And Toilet Washer/dryer Provision For Each Units</p>\r\n<p>Landmarks/Accessibility:<br />&bull; 3 Minutes walk going to University of San Carlos South Campus<br />&bull; 8-10 minutes walk to E-Mall, Downtown Malls, SSS,Cebu City Medical Hospital, South Bus Terminal<br />&bull; 8-10 University like ACT, CTS, UC, USJ-R, UV, SIT<br />&bull; 10 minutes walk to Colon St., Cebu City<br />&bull; 5 minutes drive going to Basilica Del Santo Ni&ntilde;o<br />&bull; 10 minutes drive going to Ayala Mall</p>', 'arc-tower', '2020-05-10 05:55:00', '2020-05-11 09:50:14', 'ABC Prime', 'Condominium', '3,199,145', '10,830', 0, 'Studio', NULL, 0, 'cebu', NULL, 'properties\\May2020\\Eta5GtrNUJ0abbTftp6C.jpg', 'properties\\May2020\\T0fQnt1XmEuePB7fvaNQ.jpg', 'properties\\May2020\\r92pEHOT0CFUkDdZAwMm.jpg', 'properties\\May2020\\GeLBqdFH3ISaMcQ0kOS3.jpg', 'properties\\May2020\\1Gy69G71fWJVrOWFxlwa.png', 'properties\\May2020\\45WksrVWAyj8EU4VKdBw.png', 'properties\\May2020\\BlB0uEo6EYZN6oeOi0S3.png', 'properties\\May2020\\z3dNk8H7QZ46ZWfj6xb6.png', 'properties\\May2020\\leGxeSRTH5SnXAc0cVbQ.png', 'properties\\May2020\\k514xjyVo0dtX9IcnQiE.png', 'properties\\May2020\\PA2hdPrZ0L7XZnEHor2h.jpg', 'properties\\May2020\\fHKutD9H0nCu7PTJvfD5.jpg', 0),
(13, 'Turnberry (Townhouse)', 'properties\\May2020\\VymjBojoVNB8rI0sfk4W.jpg', 'Matumbo, Pusok Road ,LapuLapu City', 3, 2, 1, '46 SQM', '88 sqm', 'ACTIVE', '<p>HOUSE DETAILS<br />▪︎ 2 Storey Townhouse<br />▪︎ 3 Bedrooms<br />▪︎ 2 Toilet And Bath<br />▪︎ 1 Powder Room<br />▪︎ Living Area<br />▪︎ Dining Area<br />▪︎ Kitchen Area<br />▪︎ Service Area<br />▪︎ Provision For Telephone &amp; Cable<br />▪︎ Carport<br /><br />AMENITIES<br />▪︎ 24 Hour Security<br />▪︎ Gated Entrance With Guardhouse<br />▪︎ Perimeter Fence<br />▪︎ Drainage System<br />▪︎ Concrete Roads<br />▪︎ Water And Electric System<br /><br />Landmarks/Accessibility:<br />▪︎ 2 Minutes Going Mactan International Airport<br />▪︎ 2 Minutes Going <span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">Marina Mall, Savemore Supermarket and Island Central Mall </span><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\"><br /></span>▪︎ 3 M<span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">inutes Drive to 2nd Mactan Bridge<br /></span>▪︎&nbsp;<span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">1 km Away from Schools, Churches, Hospitals and more!</span></p>', 'turnberry', '2020-05-10 06:45:00', '2020-05-10 09:36:53', 'Aldev Corporation', 'House & Lot', '4,699,000', '25,859', 0, 'Townhouse', NULL, 0, 'lapu-lapu', NULL, 'properties\\May2020\\yurEhi0V4vGA4MqD1Zlt.jpg', 'properties\\May2020\\E6uelhbsRFBc34glwq9Q.jpg', 'properties\\May2020\\VVjyLDQD0K2xqxtZmPof.jpg', 'properties\\May2020\\sUMLG9AMVsc5VuSwiHow.jpg', 'properties\\May2020\\Oa5fVHzYG4pbtQwBiqY5.jpg', 'properties\\May2020\\ElwqJQZFSggBEPPCZBfe.jpg', 'properties\\May2020\\jFYCMATawwqqZv2qLCdl.jpg', 'properties\\May2020\\IlfHXI8wUPRxdFmAdaMl.jpg', 'properties\\May2020\\EpgnBUkdTKzfxJfvQJ7V.jpg', 'properties\\May2020\\QkZhMD1frdS6N887ciQC.jpg', NULL, NULL, 0),
(14, 'St Francis Hills (Single Detached)', 'properties\\May2020\\OYATEA01aK6A8WY4N0rX.jpg', 'Tolo-Tolo, Consolacion', 4, 2, 2, '110 SQM', '95 SQM', 'ACTIVE', '<p>HOUSE DETAILS<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 4</span> Bedrooms<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 3</span>&nbsp;Toilet And Bath<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 2</span> Parking<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Living And Dining Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Kitchen And Service Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Porch<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Floor Area: 95 sqm<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lot Area: 110 sqm<br /><br />AMENITIES:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Landscaped Entrance Gate<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Multi-purpose Hall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Chapel<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Children&rsquo;s Playground<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Jogging Trails<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Shuttle Service Vehicles<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Complete Facilities For Drainage Power Supply<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Water Supply<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Telephone<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lighted Tree-lined Concrete Roads<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Shuttle Service Vehicles<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Perimeter Fence<br /><br />Landmarks/Accessibility:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>5 minutes from Fooda, Consolacion Church and Main Road<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 6</span>&nbsp;minutes going to City Mall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 7</span>&nbsp;minutes going to SM Consolacion<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 8</span>&nbsp;minutes going to Mendero Medical Center</p>', 'giovanni-st-francis-hills', '2020-05-10 07:23:00', '2020-05-10 09:36:33', 'Castille', 'House & Lot', NULL, NULL, 0, 'Single Dettached', NULL, 0, 'consolacion', NULL, 'properties\\May2020\\r16LTEyT8dg4vYgZRK1U.jpg', 'properties\\May2020\\mkyh8yOrE7bipJBOXNlc.jpg', 'properties\\May2020\\Uvs9k8SpAjsDt8NIAhwu.jpg', 'properties\\May2020\\ye4BZoBLraP2BwbIzfqX.jpg', 'properties\\May2020\\YGZhzwTd48dq31RN10vn.jpg', 'properties\\May2020\\5zpyq2zQ8hi9hhjOBmgw.jpg', 'properties\\May2020\\bKBl06amXu21qr5bXndT.jpg', 'properties\\May2020\\qtE1InOh8yMbuzaTZDjD.jpg', 'properties\\May2020\\YH3A9PTikoJoyzxtX9d6.jpg', 'properties\\May2020\\txiVvkn5fhjR9Quuq1Kn.jpg', 'properties\\May2020\\pYxxiLeT6wBV1IFc2ttt.jpg', 'properties\\May2020\\Y88MGrzccGQJCKqAeI0V.jpg', 0),
(15, 'Robin\'s Lane (Townhouse)', 'properties\\May2020\\WqjNQucH31ZnBpK9KcXE.jpeg', 'Nangka Purok 4 Rd, Consolacion', 3, 2, 1, '40 SQM', '56.71 SQM', 'ACTIVE', '<p>HOUSE DETAILS:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>3 Bedrooms<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>2 Toilet &amp; Bath<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Living Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Dining Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lot Area: 40sqm<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Floor Area: 56.71sqm<br /><br />AMENITIES:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ Swimming Pool<br /></span><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ Kids Playground<br /></span><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ Clubhouse<br /><br /></span>Landmarks/Accessibility:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 3</span>&nbsp;minutes going to SM Consolacion<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 4</span>&nbsp;minutes going to City Mall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>5 minutes going Fooda, Consolacion Church and Main Road<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 6</span>&nbsp;minutes going to Mendero Medical Center</p>', 'robin-s-lane', '2020-05-10 08:29:00', '2020-05-10 09:35:57', 'RTU Land', 'House & Lot', '3,200,000', '12,500', 0, 'Townhouse', NULL, 0, 'consolacion', NULL, 'properties\\May2020\\djKu2OBGb6xGh0wKPTut.jpg', 'properties\\May2020\\n556SEDzZ0Wws9UNm2Gp.jpg', 'properties\\May2020\\AYD93kZsZ5zOeWf4brPk.jpg', 'properties\\May2020\\76b24syaNbJB5uUoSbmY.jpg', 'properties\\May2020\\9ZCAe5UQNgcKr9JInBxk.jpg', 'properties\\May2020\\El9clYkayKRJrFYOVOwB.jpg', 'properties\\May2020\\14XrHONx3pIwVbl5f17R.jpg', 'properties\\May2020\\pEz0uW3ygMGkQmJtw5CJ.jpg', 'properties\\May2020\\B3S64KvUusukAY7jG2YU.jpg', 'properties\\May2020\\W5ruYCz1xAcjZm5MRstS.jpg', 'properties\\May2020\\rJ9T1g01r1knbTv21cuX.jpg', 'properties\\May2020\\qkWfKlTat79IaiggDCcB.png', 0),
(16, 'The Preston (Single Attached)', 'properties\\May2020\\fco3wKe31kZJ4L0md7zb.jpg', 'Sambag, Barangay San Vicente, Liloan', 4, 3, 2, '100 SQM', '147 SQM', 'ACTIVE', '<p>HOUSE DETAILS:<br />▪︎ 4 Bedrooms<br />▪︎ 3 Toilet And Bath<br />▪︎ Living Area<br />▪︎ Dining Area<br />▪︎ Kitchen Area<br />▪︎ Service Area<br />▪︎ Balcony<br />▪︎ Carport<br />▪︎ Lot Area: 100 Sqm And Up<br />▪︎ Floor Area: 147 Sqm (expandable)<br /><br /></p>\r\n<p>AMENITIES:<br />▪︎ 24/7 Security System<br />▪︎ Parks And Playground<br />▪︎ Covered Multi Purpose Court<br />▪︎ Swimming Pool (kids And Adult)<br />▪︎ Club House<br />▪︎ Function Hall<br /><br />Landmarks/Accessibility:<br />▪︎ 3 Minutes From Main Road<br />▪︎ 4 Minutes Going Lilaon Poblacion<br />▪︎ 4 Minutes Going Gaisano Grand Mall Liloan<br />▪︎ 8-10 Minutes Going SM Consolacion</p>', 'the-preston', '2020-05-10 08:59:00', '2020-05-10 09:35:32', 'Vanderbuilt', 'House & Lot', '5,200,000', '29,325', 1, 'Single Attached', NULL, 0, 'liloan', NULL, 'properties\\May2020\\3xODf07o4l76yP77jEzq.jpg', 'properties\\May2020\\VzOzZ5GCQwYn3o3IwLyb.jpg', 'properties\\May2020\\7xtqAA0vaOWsd1PFOJKi.jpg', 'properties\\May2020\\wbKzLhxJNNHwX1Li8h1u.jpg', 'properties\\May2020\\HExcyNOZMmyH1FMn9DfS.jpg', 'properties\\May2020\\MXuBNColfcXVjm2k2SKA.jpg', 'properties\\May2020\\9SwR30d31J0gr5ddz7HF.jpg', 'properties\\May2020\\d1W2TALsJYDTJgMi4ORM.jpg', 'properties\\May2020\\ce7aYHYzD6fBnpO8sAsH.jpg', NULL, NULL, NULL, 0),
(17, 'The Preston (Duplex)', 'properties\\May2020\\73dPBNXXs2mqHAPWaIwi.jpg', 'Sambag, Barangay San Vicente, Liloan', 3, 3, 1, '80 SQM', '117 SQM', 'ACTIVE', '<p>HOUSE DETAILS:<br />▪︎ 3 Bedrooms<br />▪︎ 3 Toilet And Bath<br />▪︎ Living Area<br />▪︎ Dining Area<br />▪︎ Kitchen Area<br />▪︎ Service Area<br />▪︎ Carport<br />▪︎ Lot Area: 80 Sqm And Up<br />▪︎ Floor Area: 117 Sqm (expandable)<br /><br /></p>\r\n<p>AMENITIES:<br />▪︎ 24/7 Security System<br />▪︎ Parks And Playground<br />▪︎ Covered Multi Purpose Court<br />▪︎ Swimming Pool (kids And Adult)<br />▪︎ Club House<br />▪︎ Function Hall<br /><br />Landmarks/Accessibility:<br />▪︎ 3 Minutes From Main Road<br />▪︎ 4 Minutes Going Lilaon Poblacion<br />▪︎ 4 Minutes Going Gaisano Grand Mall Liloan<br />▪︎ 8-10 Minutes Going SM Consolacion</p>', 'the-preston-duplex', '2020-05-10 09:54:00', '2020-05-10 09:57:34', 'Vanderbuilt', 'House & Lot', '4,500,000', '25,377', 0, 'Duplex', NULL, 0, 'liloan', NULL, 'properties\\May2020\\fSHuQHHLGO6layTAMCVU.jpg', 'properties\\May2020\\pbfBuKxRi3Q8gpf6anfF.jpg', 'properties\\May2020\\poyCkMuZhbslZqIm9lr1.jpg', 'properties\\May2020\\EsRLvLtDhCzb3M5dtedH.jpg', 'properties\\May2020\\iZVTNGvfNG4atZT3RweQ.jpg', 'properties\\May2020\\v37IkZuUI4CnmSch25V5.jpg', 'properties\\May2020\\yPKtyb3UGJUCz2N7kyXk.jpg', 'properties\\May2020\\OCCGaiWd3nxEhemzUQro.jpg', 'properties\\May2020\\Dz6yE6jJysqQscjYnH3b.jpg', NULL, NULL, NULL, 0),
(18, 'St Francis Hills (Singe Detached)', 'properties\\May2020\\8QnXvzTrNLVKTqi7VWhm.jpg', 'Tolo-Tolo, Consolacion', 3, 2, 2, '110 SQM', '70 SQM', 'ACTIVE', '<p>HOUSE DETAILS<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 3</span>&nbsp;Bedrooms<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 2</span>&nbsp;Toilet And Bath<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 2</span>&nbsp;Parking<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Living And Dining Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Kitchen And Service Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Porch<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Floor Area: 70 sqm<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lot Area: 110 sqm<br /><br />AMENITIES:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Landscaped Entrance Gate<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Multi-purpose Hall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Chapel<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Children&rsquo;s Playground<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Jogging Trails<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Shuttle Service Vehicles<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Complete Facilities For Drainage Power Supply<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Water Supply<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Telephone<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lighted Tree-lined Concrete Roads<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Shuttle Service Vehicles<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Perimeter Fence<br /><br />Landmarks/Accessibility:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>5 minutes from Fooda, Consolacion Church and Main Road<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 6</span>&nbsp;minutes going to City Mall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 7</span>&nbsp;minutes going to SM Consolacion<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 8</span> minutes going to Mendero Medical Center</p>', 'st-francis-hills-singe-detached', '2020-05-10 10:02:00', '2020-05-10 10:05:59', 'Castille', 'House & Lot', '5,314,886', '29,063', 0, 'Single Dettached', NULL, 0, 'consolacion', NULL, 'properties\\May2020\\Z47y8VmWCV5SosumG3et.jpg', 'properties\\May2020\\rPGR9j8nt63cHtIdZhtS.jpg', 'properties\\May2020\\o8Bpsoaz0kmzuuc13gPE.jpg', 'properties\\May2020\\1acMloiDxPrtkhGCYE9g.jpg', 'properties\\May2020\\FiNJS0w045lSjdbYuoZB.jpg', 'properties\\May2020\\lrgzYfdKTTGQhqnolhvG.jpg', 'properties\\May2020\\UptZIoI1QejjDgZoUlBF.jpg', 'properties\\May2020\\EkQkzqFe11M5jzyFomko.jpg', 'properties\\May2020\\uImeT1jQHkYmkSGkiUcy.jpg', 'properties\\May2020\\zjXSFluB5BD7E2NCxxsX.jpg', 'properties\\May2020\\F9nzlx3eLc3yMgpo9ycZ.jpg', 'properties\\May2020\\n5G5Hrz89RWYsy20NpTs.jpg', 0),
(19, 'Casa Mira Towers', 'properties\\May2020\\yDbEJ8Gv2S7fr8Xy77hF.jpg', 'Alang-Alang, Mandaue City', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>AVAILABLE UNIT:<br />&bull; Studio Without Balcony<br />&bull; Sdudio With Balcony<br />&bull; 1 Bedroom With Balcony<br /><br />AMENITIES:<br />&bull; Gate &amp; Guardhouse<br />&bull; Perimeter Fencing<br />&bull; Multi-level Clubhouse with Lap Pool<br />&bull; Gym<br />&bull; Lobby Lounge<br />&bull; Pavillion with Swimming Pool<br />&bull; Zen Garden<br />&bull; Pedestrian Pathways &amp; Jogging Paths<br />&bull; Outdoor Lounges<br />&bull; Retail Area<br />&bull; Designated Parking Spaces<br />&bull; 24 Hour CCTV Security System</p>\r\n<p>UNIT DELIVERABLE:<br />&bull; Floor Tiles<br />&bull; Painted Wall<br />&bull; Table &amp; Bath With Ceramic Non Slip Floor Tiles<br />&bull; Bathroom Fixtures<br />&bull; Kitchen Base Cabinet With Countertop<br />&bull; Kitchen Exhaust<br />&bull; Provision For Cable TV, Telephone/Internet<br />&bull; Installed Fire Detector and Suppression System<br />&bull; Aircon Provision<br />&bull; Shower Heater Provision&nbsp;<br /><br />Landmarks/Accessibility:<br />&bull; Near Pacific Mall Mandaue<br />&bull; Near Maayo Hotel<br />&bull; Near Old and New Mactan Bridge<br />&bull; Near Prince Warehouse Club</p>', 'casa-mira-towers', '2020-05-10 21:02:00', '2020-05-10 21:38:46', 'Cebu Land Master', 'Condominium', '2,195,752', '4,7,664', 1, 'Studio', NULL, 0, 'mandaue', NULL, 'properties\\May2020\\6b3QTnvZfdezU6kB719X.jpg', 'properties\\May2020\\nhBrpu4FNaSsImnwHxm7.jpg', 'properties\\May2020\\te5ed8MsORYCFi1tqQeW.jpg', 'properties\\May2020\\fZhdogVNk0WKS2lm4FtK.jpg', 'properties\\May2020\\t5R0zqoDyC7NDYG0Jz6P.jpg', 'properties\\May2020\\37zoJvTfrL4XoEQggsHO.jpg', 'properties\\May2020\\P6YYSwUYa5eNCF8oXltF.jpg', 'properties\\May2020\\1VwrSESBQIGe6gkJVNnJ.jpg', 'properties\\May2020\\1Anp7LIrOrVQ00a5VVb7.jpg', 'properties\\May2020\\H8VGCPTzbOeTKdL6KaQR.jpg', 'properties\\May2020\\5mB3ipjUgjO04cHoBsiH.jpg', 'properties\\May2020\\kPOH7kIBj1NLJEcemIlr.jpg', 0),
(20, 'Saekyung', 'properties\\May2020\\OYVZ6mwwpocjIUSYNB1v.jpg', 'Looc, Lapu-Lapu', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>CONDO DETAILS:<br />&bull; Studio Unit With Balcony<br />&bull; Floor Area: 27 SQM<br /><br />AMENITIES:<br />&bull; 24 Hour Security<br />&bull; Gated Entrance With Guardhouse<br />&bull; Swimming Pool<br />&bull; Basketball Court (convertible For Badminton, Volleyball Etc.)<br />&bull; Playground<br /><br />UNIT DELIVERABLE:<br />&bull; Kitchen&nbsp; Cabinets With&nbsp; Granite Countertop , Smooth Wall Finish<br />&bull; Finished Painted Interior Wall<br />&bull; Complete Electrical Switches And Convenient Outlet<br />&bull; Tiled Floors, Tiled Toilet And Bath With&nbsp; Fixtures<br />&bull; Provision For Tv And Telephone Line<br /><br />Landmarks/Accessibility:<br />&bull; Near Opon Mercado and Birhen sa Regla Church<br />&bull; 5 Minuts Going Mactan Bridge&nbsp;<br />&bull; 10 Minutes Going Mactan Internation Airport&nbsp;</p>', 'saekyung', '2020-05-11 01:08:00', '2020-05-11 04:31:09', 'Priland', 'Condominium', '1,650,000', '9,780', 0, 'Studio', NULL, 1, 'lapu-lapu', NULL, 'properties\\May2020\\RmS7rBvtWhuoGwrsBktO.jpg', 'properties\\May2020\\4gEBQOlDTGYGAHT8sMH5.jpg', 'properties\\May2020\\hgn5B8WeXIbRzSdIh6D4.jpg', 'properties\\May2020\\KMZMlGSV32D41afKGnpi.jpg', 'properties\\May2020\\zfpu1elHv2RbQR94LpWe.jpg', 'properties\\May2020\\OLbcOFmuygb6alADCRts.jpg', 'properties\\May2020\\XtKCAJdC8SYLbciBBNlH.jpg', 'properties\\May2020\\v3MHCR3QEOg5RTrCJpzB.jpg', 'properties\\May2020\\5oeGh5TrjTogDpOVDfM3.jpg', 'properties\\May2020\\yxEJeG0VCx7hcGRA8949.jpg', 'properties\\May2020\\EEInL3f7KZClgSR4ZFTm.jpg', NULL, 0);
INSERT INTO `properties` (`id`, `title`, `image`, `address`, `bedrooms`, `toilet_bath`, `parking`, `lot_area`, `floor_area`, `status`, `body`, `slug`, `created_at`, `updated_at`, `developer`, `property_type`, `price`, `monthly`, `featured`, `type`, `model_type`, `rfo`, `city`, `vtour`, `image_1`, `image_2`, `image_3`, `image_4`, `image_5`, `image_6`, `image_7`, `image_8`, `image_9`, `image_10`, `image_11`, `image_12`, `order`) VALUES
(21, 'Belize North (Duplex)', 'properties\\May2020\\JXAawUR50DQShNrWIUK7.jpg', 'Nangka, Consolacion, Cebu', 3, 3, 3, '75 SQM', '93 SQM', 'ACTIVE', '<p>HOUSE DETAILS<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 3</span>&nbsp;Bedrooms<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 3</span>&nbsp;Toilet &amp; Bath<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Living And Dining Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Kitchen And Service Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Carpark<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Floor Area : 75 sqm<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lot Area : 93 sqm<br /><br />AMENITIES:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Ample Guest Parking<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Basketball Court<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Clubhouse<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Children&rsquo;s Playground<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Jogging Trails<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Landscaped Gardens And Hardscapes<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Street Lights<br /><br /><br />Landmarks/Accessibility:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 4</span>&nbsp;minutes from Fooda, Consolacion Church and Main Road<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 5</span>&nbsp;minutes going to City Mall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 6</span>&nbsp;minutes going to SM Consolacion<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 7</span> minutes going to Mendero Medical Center</p>', 'belize-north-duplex', '2020-05-11 04:48:00', '2020-05-11 07:17:11', 'Priland', 'House & Lot', '4,748,820', '23,268', 0, 'Duplex', NULL, 0, 'consolacion', NULL, 'properties\\May2020\\mgZs8DyfDE4pw6iDk1g4.jpg', 'properties\\May2020\\kVoN5W0EbS8XWrBdC4nB.jpg', 'properties\\May2020\\iLJR5EeatelVUk21axoS.jpg', 'properties\\May2020\\hZFyDQ3dhfydGZhwWMh1.png', 'properties\\May2020\\P7YTFjbENofi7bQf9ejy.jpg', 'properties\\May2020\\eaNEZyxQ2L74Ka5bQMJU.PNG', 'properties\\May2020\\lEejGleGeABvEIkf3xtS.jpg', 'properties\\May2020\\xpO4A5JOscI8VSEALHUj.jpg', 'properties\\May2020\\Idvm4smUvwik1bBNEzLF.jpg', NULL, NULL, NULL, 0),
(22, 'Breeza Scapes (Attached)', 'properties\\May2020\\ahY7JzCpNAO86uNyhAFQ.jpg', 'Locata Rd, Lapu-Lapu City', 4, 3, 2, '101.25 SQM', '134 SQM', 'ACTIVE', '<p>HOUSE DETAILS:<br />3 Bedrooms<br />1 Guest Room<br />2 Toilet And Bath<br />1 Comfort Room<br />Living And Dining Area<br />Kitchen And Service Area<br />Garden Area<br />2 Carport<br />Lot Area: 101.25 sqm<br />Floor Area: 134 sqm<br /><br />AMENITIES:<br />24 Hour Security<br />Gated Entrance With Guardhouse<br />Clubhouse<br />Basketball Court<br />Playground<br />Drainage System</p>', 'breeza-scapes-attached', '2020-05-11 07:24:00', '2020-05-11 07:59:47', 'Priland', 'House & Lot', '7,122,174', '28,009', 0, 'Single Attached', NULL, 0, 'lapu-lapu', NULL, 'properties\\May2020\\hdn3gXeW4q6W9Ks03jPD.jpg', 'properties\\May2020\\1AupUwGkmLmpKiD4yJR5.jpg', 'properties\\May2020\\NKmcwLIQqti0SHgwHq6S.jpg', 'properties\\May2020\\Ipc1GscmBwcK13MP5xo4.jpg', 'properties\\May2020\\pYNtBa5YLvVi6N5Rx7pH.jpg', 'properties\\May2020\\IbQgcJzYthNm0Vv0qSA4.jpg', 'properties\\May2020\\K2uBQfRq2yv2Cs32dVzy.jpg', 'properties\\May2020\\J5MNuyDjTokxkd1I7T37.jpg', 'properties\\May2020\\vCWI30UaZ959Hhr3r6K5.jpg', NULL, NULL, NULL, 0),
(23, 'Breeza Scapes (Duplex)', 'properties\\May2020\\zqb3WfyLUk7yfCM4f93M.jpg', 'Locata Rd, Lapu-Lapu City', 4, 3, 2, '101 SQM', '134 SQM', 'ACTIVE', '<p>HOUSE DETAILS:<br />3 Bedrooms<br />1 Guest Room<br />2 Toilet And Bath<br />1 Comfort Room<br />Living And Dining Area<br />Kitchen And Service Area<br />Garden Area<br />2 Carport<br />Lot Area: 101 sqm<br />Floor Area: 134 sqm<br /><br />AMENITIES:<br />24 Hour Security<br />Gated Entrance With Guardhouse<br />Clubhouse<br />Basketball Court<br />Playground<br />Drainage System</p>', 'breeza-scapes-duplex', '2020-05-11 08:13:12', '2020-05-11 08:13:12', 'Priland', 'House & Lot', '7,122,174', '28,009', 0, 'Duplex', NULL, 0, 'lapu-lapu', NULL, 'properties\\May2020\\ZSPZvAywvIjRs9duEcpR.jpg', 'properties\\May2020\\kwgZ1bObCmO6Tm3vEudR.jpg', 'properties\\May2020\\4QgZF1PjSnGPOxmCiLXn.jpg', 'properties\\May2020\\yj18tPAlBiF41xVY9ZXM.jpg', 'properties\\May2020\\t6uWSJo7KkxYeR2bpsJj.jpg', 'properties\\May2020\\QUgQvityEuK1XJAdm7Np.jpg', 'properties\\May2020\\PRf9LXvxICNzM2kYJbQw.jpg', 'properties\\May2020\\noXKYNLnp1478CuA2Q9H.jpg', NULL, NULL, NULL, NULL, 0),
(24, 'Taft East Gate', 'properties\\May2020\\EFathFBw6QR9hi34LXa9.jpg', 'Cardinal Rosales Ave, Cebu City', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>AVAILABLE UNIT:<br />&bull; Studio Without Balcony<br />&bull; Studio With Balcony<br />&bull; 1 Bedroom With Balcony<br />&bull; 2 Bedrooms Wuth Balcony<br /><br />CONDO DETAILS:<br />&bull; Floor Area: 29.58sqm<br />&bull; Bed Area<br />&bull; Living And Dining Area<br />&bull; Kitchen And Service Area<br />&bull; Wooden Main Panel Door<br />&bull; Vinyl Floor Tiles<br />&bull; Kitchen Counter With Cabinet And Sink<br />&bull; Toilet And Bathroom Door<br />&bull; Bathroom Ceramic Floor Tiles<br />&bull; 1.5m Ceramic Wall Tiles At Shower Area<br />&bull; Painted Wall Finished<br />&bull; Provision For Telephone And Cable Tv Connection<br />&bull; Fire Detection And Alarm System<br /><br />AMENITIES:<br />&bull; 4 High-speed Elevators<br />&bull; Back-up Power<br />&bull; Automatic Transfer System(ats)<br />&bull; Fire Protection Features<br />&bull; Fire Detection &amp; Alarm System (fdas)<br />&bull; Fdas Annunciator<br />&bull; Sewage Treatment Plant<br />&bull; Property Management<br />&bull; Retail Podium<br />&bull; Cctv<br />&bull; Multipurpose Sports Area<br />&bull; Swimming Pool<br />&bull; Fitness Gym<br />&bull; Function Hall<br />&bull; Landscape Gardens<br />&bull; Food And Beverage Terraces<br />&bull; Sky Plaza<br /><br />Landmarks/Accessibility:<br />&bull; Across Landers Superstore<br />&bull; Walking Distance to Ayala Mall<br />&bull; Walking Distance to Cebu Business Park<br />&bull; Near IT Park<br /><br /></p>\r\n<p>&nbsp;</p>', 'taft-east-gate', '2020-05-11 09:00:00', '2020-05-11 09:07:16', 'Taft Properties', 'Condominium', '3,250,000', NULL, 0, 'Studio', NULL, 0, 'cebu', NULL, 'properties\\May2020\\d0orCx2NAbICdCmtkgoj.jpg', 'properties\\May2020\\ZXhGbDbNvwCZRfg8Ez3d.jpg', 'properties\\May2020\\w2VXsFgrxssogtDb5fZS.jpg', 'properties\\May2020\\oyeu0y7dw0ahoKDGcNXd.jpg', 'properties\\May2020\\VbZRrFKiFK2lNhZwuBiC.jpg', 'properties\\May2020\\2VJODx6wX23Pbh97KUdY.jpg', 'properties\\May2020\\quEhKefPwhdlSctkXQyS.jpg', 'properties\\May2020\\LM0FZBIXxwBUGZj969gh.jpg', 'properties\\May2020\\yLoVR8ODlgddqNiBg11e.jpg', 'properties\\May2020\\W23XWC2Vd89X0ZDH8fdq.jpg', 'properties\\May2020\\ZglxlOKfqYlf56d2C7OI.jpg', 'properties\\May2020\\6Q9smdOkrx1eG2lVuEY4.jpg', 0),
(25, 'Symfoni Nichols', 'properties\\May2020\\TLA9VI5NdSu5TckFKErr.jpg', 'Nicols Park, Guadalupe Cebu', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>AMENITIES:<br />&bull; Landscaped Gardens<br />&bull; 24 Hour Security<br />&bull; Bacik-up Power<br />&bull; Strategically Loacated Cctvs<br />&bull; Fire Protection Features<br />&bull; Property Management<br />&bull; Elevators<br />&bull; 1 Service Elevators<br />&bull; Swimming Pool<br />&bull; Multi Purpose Hall<br />&bull; Childrens Play Area<br />&bull; Fitness Gym<br />&bull; Game Room<br />&bull; Study Room<br />&bull; Landscaped Gardens<br />&bull; Basketball Court<br /><br />UNIT DELIVERABLE:<br />&bull; Floor Area: 20 Sqm<br />&bull; 1 Toilet And Bath<br />&bull; Living And Dining Area<br />&bull; Kitchen Area<br />&bull; Wooden Main Panel Door<br />&bull; Vinyl Floor Tiles<br />&bull; Kitchen Counter With Cabinet And Sink<br />&bull; Toilet And Bathroom Door<br />&bull; Athroom Ceramic Floor Tiles<br />&bull; 1.5m Ceramic Wall Tiles At Shower<br />&bull; Painted Wall Finished<br />&bull; Provision For Telephone And Cable Tv Connection<br />&bull; Fire Detection And Alarm System<br /><br /></p>', 'symfoni-nichols', '2020-05-11 09:46:00', '2020-05-11 09:48:52', 'Taft Properties', 'Condominium', '2,399,760', '6,888', 0, 'Studio', NULL, 0, 'cebu', NULL, 'properties\\May2020\\0R0GIG1iavW1nbiXq2P3.png', 'properties\\May2020\\ATOeZfo4GSAppQeYDYoH.png', 'properties\\May2020\\G71s098eWCkzNlRgpZYp.jpg', 'properties\\May2020\\6TbdwlePe4dkc88rY8tc.jpg', 'properties\\May2020\\CtIQSTyNX17ENlUDqFnM.jpg', 'properties\\May2020\\17K8h7wYsgoKLUM3Oyqc.jpg', 'properties\\May2020\\HNK1CzVxXLfjRWslpxq6.jpg', 'properties\\May2020\\Z9v6n0YVABSRt4nZ2OJj.jpg', NULL, NULL, NULL, NULL, 0),
(26, 'Vertex Coast', 'properties\\May2020\\TP3Vj6Hlu9hb5E2yJOwS.jpg', 'Punta Engaño Lapulapu City', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>AVAILABLE UNIT\"<br />&bull; SOHO: Studio &amp; 1 Bedroom<br />&bull; Residential: Studio, 1 &amp; 2 Bedrooms<br /><br />CONDO DETAILS:<br />&bull; Bed Area<br />&bull; 1 Toilet And Bath<br />&bull; Living Area<br />&bull; Dining Area<br />&bull; Kitchen Area<br />&bull; Provisions For Aircon<br />&bull; Provisions For Telephone<br />&bull; Provisions For Cable<br />&bull; Provisions For Induction Stove<br /><br />AMENITIES:<br />&bull; 24 Hour Security<br />&bull; Reception Area<br />&bull; 8 Elevators Shared By Both Towers<br />&bull; Central Garbage Collection Area<br />&bull; Provision For Garbage Collection On Every Floor<br />&bull; Tower 1 Lobby At Ground Floor<br />&bull; Tower 2 Lobby At Ground Floor</p>\r\n<p>&bull; Spacious Lobby At Amenity Floor<br />&bull; Kiddie Pool<br />&bull; Infinity Pool<br />&bull; GymGarden Deck<br />&bull; Function Room<br />&bull; Commercial Spaces<br />&bull; Game Room<br /><br />LOCATION:<br />&bull; Minutes away from resorts, commercial establishment and the Mactan International Airport, Vertex Coast sits in easily accessible and highly coveted location in Mactan.</p>', 'vertex-coast', '2020-05-12 08:34:00', '2020-05-12 08:57:29', 'Priland', 'Condominium', '3,700,291', NULL, 1, 'Studio', NULL, 0, 'lapu-lapu', NULL, 'properties\\May2020\\1GwDJbm4Gczv0XPQYBOr.jpg', 'properties\\May2020\\B1XWjug8FSguTzQ8l42i.jpg', 'properties\\May2020\\LHbWWEN4OCRVjzraClzq.jpg', 'properties\\May2020\\URBB4IPo0OqQMSev3PzY.jpg', 'properties\\May2020\\fySYGNIXcBpL8k30Daw3.jpg', 'properties\\May2020\\XyyArTkpsoBZ1CsR8iDh.jpg', 'properties\\May2020\\LT6Z94aFduDi1D79n12K.jpg', 'properties\\May2020\\8sRzgDyblLloqqPN4iNQ.jpg', 'properties\\May2020\\DrkINlIQhrMlGZgwvSS6.jpg', 'properties\\May2020\\qEeason83xVzO0GPlOD9.jpg', 'properties\\May2020\\oK04NlQdm8ADIYjBR4EJ.jpg', 'properties\\May2020\\rfbHpYf7VD56Yl1mMVfs.jpg', 0),
(27, 'Vertex Central', 'properties\\May2020\\AUiDmmnjf3klybpeh7cW.jpg', 'Archbishop Reyes Avenue Cebu', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>AVAILABLE UNIT:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>SOHO<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Studio<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>1 Bedroom<br /><br />CONDO DETAILS:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Bedroom Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Dining Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Kitchen Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Toilet And Bath<br /><br />AMENITIES<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>24 Hour Security<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Lobby<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Gym<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Sky Garden- Exclusive For Penthouse<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Pool And Lounge Deck<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Commercial Spaces<br /><br />Landmarks/Accessibility:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Walking distance to Ayala Mall<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Walking distancto to IT park<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>walking distance to Cebu Business Park<br /><br /></p>', 'vertex-central', '2020-05-12 09:43:00', '2020-05-12 09:52:48', 'Priland', 'Condominium', '4,414,197', NULL, 0, 'Studio', NULL, 0, 'cebu', NULL, 'properties\\May2020\\qaGJtPPZ85SEJdMcpF8f.jpg', 'properties\\May2020\\TkvPp75usPc4jtfoRxiS.jpg', 'properties\\May2020\\vaVsrB8Guc30MNAOyneU.jpg', 'properties\\May2020\\8hpVgsqGyHu02qtQCI9s.jpg', 'properties\\May2020\\Kq1GY7eR4hmFXNCEuMTG.jpg', 'properties\\May2020\\rABjyRbnFBQbDofkXOHk.jpg', 'properties\\May2020\\9DCYJMELNt4g9eRZqBbx.jpg', NULL, NULL, NULL, NULL, NULL, 0),
(28, 'Paseo Grove', 'properties\\May2020\\amjPXPL4ZqqFxFLKYXkL.jpg', 'Humay-Humay, Lapu-Lapy City', NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '<p>AVAILABLE UNIT:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ Studio<br /></span><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 1 Bedroom<br /></span><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ 2 Bedrooms</span><br /><br />CONDO DETAILS:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>1 Toilet And Bath<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Living And Dining Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Kitchen Area<br /><br />AMENITIES:<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Swimming Pool With Pavilion<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Playground<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Parks &amp; Garden Area<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Amenity Deck<br /><span style=\"color: #050505; font-family: \'Segoe UI Historic\', \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 15px; white-space: pre-wrap;\">▪︎ </span>Commercial Area<br /><br />Landmarks/Accessibility:<br /><span class=\"fgm26odu nvdbi5me oygrvhab ditlmg2l kvgmc6g5 knj5qynh tbxw36s4 q9uorilb\" style=\"margin: 0px 1px; vertical-align: text-bottom; display: inline-block; font-family: inherit;\">▪︎ Near&nbsp; </span>Pueblo Verde<br /><span class=\"fgm26odu nvdbi5me oygrvhab ditlmg2l kvgmc6g5 knj5qynh tbxw36s4 q9uorilb\" style=\"margin: 0px 1px; vertical-align: text-bottom; display: inline-block; font-family: inherit;\">▪︎&nbsp; </span>Beside Mepz 11<br /><span class=\"fgm26odu nvdbi5me oygrvhab ditlmg2l kvgmc6g5 knj5qynh tbxw36s4 q9uorilb\" style=\"margin: 0px 1px; vertical-align: text-bottom; display: inline-block; font-family: inherit;\">▪︎&nbsp; </span>Near Malls and Hospital<br /><span class=\"fgm26odu nvdbi5me oygrvhab ditlmg2l kvgmc6g5 knj5qynh tbxw36s4 q9uorilb\" style=\"margin: 0px 1px; vertical-align: text-bottom; display: inline-block; font-family: inherit;\">▪︎&nbsp; </span>5 Minutes Going Mactan International Airport</p>', 'paseo-grove', '2020-05-12 10:18:00', '2020-05-12 10:32:01', 'Priland', 'Condominium', '2,310,417', NULL, 0, 'Studio', NULL, 0, 'lapu-lapu', NULL, 'properties\\May2020\\nk6MltAWmvdSikRuegcU.jpg', 'properties\\May2020\\4c8NgElg0vjfHrNiZxmm.jpg', 'properties\\May2020\\mPtxXB81jLAF2xVYJc4S.jpg', 'properties\\May2020\\P3uvSr4bWPI6aYzWJXNb.jpg', 'properties\\May2020\\oIJa49kLYAWyztbKLatS.jpg', 'properties\\May2020\\TIRchXXfehEy1Ig1n3Gu.jpg', 'properties\\May2020\\lDM79pxC6kvgKw5zY6Bn.jpg', 'properties\\May2020\\aKZVX8M4Ak3hJSW313MV.jpg', 'properties\\May2020\\4bmxYgbA9vRA3C79mwPJ.jpg', 'properties\\May2020\\Ydnew2GHMYxizSpedmhe.jpg', 'properties\\May2020\\5zOYAb6f4ol77jHPQWZA.jpg', 'properties\\May2020\\RKQeq46JVhqiK3e6oQ8H.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-04-07 00:31:41', '2020-04-07 00:31:41'),
(2, 'user', 'Normal User', '2020-04-07 00:31:41', '2020-04-07 00:31:41');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'House and Condominium', '', 'text', 1, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings/May2020/F0g9J53AqsNJq2dNBgeK.png', '', 'image', 4, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 24, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'contact.phone', 'phone', '09179516517', NULL, 'text', 6, 'Contact'),
(12, 'contact.office', 'office', 'Unit 11 2nd floor The Green Strip S.E, S Jayme St, Mandaue City, 6014', NULL, 'text', 7, 'Contact'),
(13, 'contact.facebook', 'facebook', 'https://www.facebook.com/HouseAndCondominiumInCebu/', NULL, 'text', 8, 'Contact'),
(14, 'contact.messenger', 'Messenger', 'https://www.messenger.com/t/HouseAndCondominiumInCebu/', NULL, 'text', 9, 'Contact'),
(15, 'contact.email', 'Email Addres', 'herdarnel@gmail.com', NULL, 'text', 10, 'Contact'),
(16, 'contact.mail_name', 'Mail Name', 'Arnel Herda', NULL, 'text', 11, 'Contact'),
(17, 'agent-info.name', 'name', 'Arnel Herda', NULL, 'text', 12, 'Agent Info'),
(18, 'agent-info.image', 'Image', 'settings/May2020/WGnqE5WtaATU7d3ATYO8.jpg', NULL, 'image', 15, 'Agent Info'),
(20, 'agent-info.contact_number', 'Contact Number', '+(63) 9179516517', NULL, 'text', 16, 'Agent Info'),
(21, 'agent-info.facebook_page', 'Facebook Page', 'HouseandCondominiumInCebu', NULL, 'text', 17, 'Agent Info'),
(22, 'agent-info.brokerage', 'Brokerage', 'settings/May2020/YGRuFIChhXewNa4EwTmj.png', NULL, 'image', 18, 'Agent Info'),
(23, 'agent-info.position', 'Position', 'Property Sales Manager', NULL, 'text', 13, 'Agent Info'),
(24, 'contact.instagram', 'Instagram', NULL, NULL, 'text', 19, 'Contact'),
(25, 'contact.youtube', 'Youtube', NULL, NULL, 'text', 20, 'Contact'),
(26, 'site.site_description', 'Site Description', '<p>&copy; 2020 House and Condominium. All rights reserved. <a class=\"site-credit-link\" href=\"/page/site-credits\" target=\"_blank\" rel=\"noopener\">Site Credits</a></p>', NULL, 'rich_text_box', 22, 'Site'),
(27, 'site.site_meta', 'Site Meta', 'If you are Looking for a property in Cebu, check our listing and lets talk about it!', NULL, 'text_area', 23, 'Site'),
(29, 'site.favicon', 'Site Favicon', 'settings/May2020/lqJroiOVdnyD16qhK94j.png', NULL, 'image', 21, 'Site'),
(30, 'site.site_image', 'Site Meta Image', 'settings/May2020/T51ZLXvoQ40fqhOKdLu7.png', NULL, 'image', 25, 'Site');

-- --------------------------------------------------------

--
-- Table structure for table `sold_properties`
--

CREATE TABLE `sold_properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sold_properties`
--

INSERT INTO `sold_properties` (`id`, `title`, `client_name`, `city`, `created_at`, `updated_at`, `image`) VALUES
(1, 'Plumera Condominium', 'Jenny Anderson', 'Lapu-Lapu', '2020-05-04 20:52:00', '2020-05-04 22:29:44', 'sold-properties/May2020/pqCeXh8c21YT0rXOoSdF.jpg'),
(2, 'Saekyung 956', 'Joseph Smith', 'Lapu-Lapu', '2020-05-04 20:54:00', '2020-05-04 22:29:35', 'sold-properties/May2020/fvcytvGQC0VjjWnAZj41.jpg'),
(3, 'Francesco, St Francis Hills', 'John Doe', 'Consolacion', '2020-05-04 20:54:00', '2020-05-04 22:29:25', 'sold-properties/May2020/Ke1jhmXSxNCp8OyaH6Vl.jpg'),
(4, 'City Clou', 'Christian Grey', 'Cebu', '2020-05-04 20:55:00', '2020-05-04 22:29:15', 'sold-properties/May2020/oTcx6YRRW0xHXEwYvRg4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(265) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(265) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `image`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Maureen Seguerra', 'testimonials/May2020/Z4eJwlUWAZvlXup9HSaF.jpg', 'It\'s my long overdue plan to own a house in Cebu but it was delayed because I couldn\'t decide the perfect design and location for me.Good thing I met Arnel and he introduce to me the best house and condo projects in Cebu. The most detailed info per project was layed out to me such as the developers, the most accessible locations and designs that are heart stopping. Finally, ai was able to decide which one and everything runs smoothly...Thanks for the help my friend.', '2020-05-05 00:09:34', '2020-05-05 00:09:34'),
(2, 'Razzel Ann Arreglo', 'testimonials/May2020/ruCittPuref9SDMtZfxN.jpg', 'Arnel is very personable, communicates exceptionally well and works very hard to get you the results you are looking for. I look forward to working with Arnel as we continue looking into this project and would recommend him to anyone needing commercial realty services.', '2020-05-05 00:11:26', '2020-05-05 00:11:26'),
(3, 'Elmer John Dave', 'testimonials/May2020/LpQQkE9UvMMgSi9MxKF4.jpg', 'Arnel never settle for less, always willing to go the extra mile and has a very positive attitude which makes him very approachable. He always equip himself with the necessary tools, skills and knowledge to be the best he can be. Recommended.', '2020-05-05 00:12:22', '2020-05-05 00:12:22'),
(4, 'Steph Quimco', 'testimonials/May2020/0xSLrRE5AQ3QnMFyeklk.jpg', 'Sir Arnel is a very good home match maker...He would do the extra mile to assist you with all the papers needed. Highly recommended. 👏', '2020-05-05 00:13:57', '2020-05-05 00:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(22, 'menu_items', 'title', 13, 'pt', 'Publicações', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(24, 'menu_items', 'title', 12, 'pt', 'Categorias', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(25, 'menu_items', 'title', 14, 'pt', 'Páginas', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-04-07 00:55:14', '2020-04-07 00:55:14'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-04-07 00:55:14', '2020-04-07 00:55:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'arnelherda', 'herdarnel@gmail.com', 'users/default.png', NULL, '$2y$10$ry4Q.apc0tKH7WOwnpK8Ium.JNaLbsmAzyzj5OhremLAKu/7gfJ8u', NULL, NULL, '2020-04-07 00:34:28', '2020-04-07 00:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `link`, `thumbnail`, `created_at`, `updated_at`) VALUES
(1, 'Antara Talisay Condominium', 'QYdtIgr0QK4', 'videos/May2020/VxIEUekRZzjJEkXW5XbL.jpg', '2020-05-04 23:39:21', '2020-05-04 23:39:21'),
(2, 'Plumera Condominium', '1chy1Y_MLOA', 'videos/May2020/mLnpsKqljpel1MwmUQIU.jpg', '2020-05-04 23:39:00', '2020-05-04 23:40:17'),
(3, 'Saekyung 956', 'OWqxMrXZDNQ', 'videos/May2020/w0YelsZPNlbjfUt6dMFg.jpg', '2020-05-04 23:40:39', '2020-05-04 23:40:39'),
(4, 'City Clou', 'I7aOOPcZDGo', 'videos/May2020/u6q0LJnRh9PPgGNCjI1S.jpg', '2020-05-04 23:41:04', '2020-05-04 23:41:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `developers`
--
ALTER TABLE `developers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `properties_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `sold_properties`
--
ALTER TABLE `sold_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `developers`
--
ALTER TABLE `developers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `sold_properties`
--
ALTER TABLE `sold_properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
