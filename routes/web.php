<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::any('/', function () {
//     return view('welcome');
// });

Route::any('/','HomeController@home')->name('home');
Route::any('/about','HomeController@about')->name('about');
Route::any('/videos','HomeController@videos')->name('videos');
Route::any('/post/{slug}','HomeController@post')->name('post');
Route::any('/page/{slug}','HomeController@page')->name('page');
Route::any('/contact','ContactController@index')->name('contact.index');
Route::post('/contact','ContactController@contactStore')->name('contact.store');
Route::any('/search','PropertyController@search')->name('search');
Route::any('/property/{category}/{slug}','PropertyController@property')->name('property');
Route::any('/property/{category}','PropertyController@category')->name('category');
Route::any('/house-and-lot','PropertyController@houseLot')->name('houselot');
Route::any('/house-and-lot/{city}','PropertyController@houseLotCity')->name('housecity');
Route::any('/condominium','PropertyController@condominium')->name('condominium');
Route::any('/condominium/{city}','PropertyController@condominiumCity')->name('condominiumcity');
Route::any('/rfo','PropertyController@rfo')->name('rfo');
Route::any('/rfo/{type}','PropertyController@rfoType')->name('rfoType');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
