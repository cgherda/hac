@extends('layouts.master')
@section('content')
  @foreach ($page as $content)
    @section('title', $content->title)
    @section('image', Voyager::image( setting('site.site_image') ))
     <div class="contactus-banner single-banner" style="background-image: url('{{ Voyager::image( $content->image ) }}');">
       <div class="overlay"></div>
       <div class="container">
        <div class="single-banner-title">
           <h1>{{ $content->title }}</h1>
         </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $content->title }}</li>
          </ol>
        </nav>
      </div>
     </div>
   
    <section class="video-page">
     <div class="container">
        <div class="row">
          @foreach ($videos as $video)
            <div class="col-12 col-sm-6 col-md-6 col-lg-3">
            <div class="card mb-4 video-item box-shadow property-item">
              <div class="thumbnail"  style="background-image: url({{ Voyager::image( $video->thumbnail ) }});">
                  <a  data-fancybox href="https://www.youtube.com/watch?v={{$video->link}}" >
                    <span class="youtube">
                      <span class="icon-play"></span>
                    </span>
                  </a>
              </div>
              <div class="card-body">

                <div class="details">
                  <p class="name">{{$video->title}}</p>
                </div>
              </div>
            </div>

            </div>
          @endforeach
        </div>
      </div>  
    </section>
  @endforeach
@endsection