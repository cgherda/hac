<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">

    <!-- Twitter -->
    <meta name="twitter:site" content="{{ setting('site.title') }}">
    <meta name="twitter:title" content="@yield('title') | {{ setting('site.title') }}">
    <meta name="twitter:description" content="{{ setting('site.site_meta')}}">
    <meta name="twitter:image" content="@yield('image')">

    <!-- Facebook -->
    <meta property="og:url" content="{{ URL::current() }}">
    <meta property="og:title" content="@yield('title') | {{ setting('site.title') }}">
    <meta property="og:description" content="{{ setting('site.site_meta')}}">

    <meta property="og:image" content="@yield('image')">
    <meta property="og:image:secure_url" content="@yield('image')">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <meta property="og:title" content="@yield('title') | {{ setting('site.title') }}">
    <meta property="og:description" content="{{ setting('site.site_meta')}}">
    <meta property="og:image" content="@yield('image')">

    <meta property="og:url" content="http://houseandcondominium.com">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{ Voyager::image( setting('site.favicon') ) }}" type="image/x-icon">
    <link rel="stylesheet" rel="preload" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" rel="preload" href="{{asset('css/app.css')}}">
    {{-- <link rel="stylesheet" href="{{asset('css/fonts.css')}}"> --}}
    {{-- <link rel="stylesheet" href="{{asset('css/style.css')}}"> --}}
    <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>
  @if (\Route::current()->getName() != 'property') 
    <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5ecb72920ea83f0012d8185c&product=sticky-share-buttons&cms=sop" async="async"></script>
  @endif
  <!-- ManyChat -->
  <script src="//widget.manychat.com/324644718354851.js" async="async"></script>

</head>
<body>
<div class="page">
    <!-- Page Header-->
    @include('partials.header')
    <!-- Swiper-->
    @yield('content')
    <!-- Footer-->
    @include('partials.footer')

</div>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/jquery.fancybox.min.js')}}"></script>

@yield('js')

<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
