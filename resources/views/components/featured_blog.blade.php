<section class="section yellow-section">
	<div class="container">
		<h3 class="section-title text-center">Buyer Tips</h3>
		<div class="row">
			{{-- @for ($i = 0; $i < 3 ; $i++) --}}
			@foreach ($posts as $post)
				<div class="col-12 col-sm-6 col-lg-3">
					<div class="flip-container blog-item" ontouchstart="this.classList.toggle('hover');">
						<div class="flipper">
							<div class="front" style="background-image: url({{ Voyager::image( $post->image ) }});">
								<p>{{$post->title}}</p>
							</div>
							<div class="back">
								<!-- <h3>{{$post->title}}</h3> -->
								<p>{!!$post->body!!}</p>
								<!-- <a href="{{ route('post',$post->slug) }}">Read More</a> -->
							</div>
						</div>
					</div>
				</div>
			@endforeach
			{{-- @endfor --}}
		</div>
	</div>
</section>