  <section class="section awards-section">
    <div class="container">
      <div class="text-center">
        <h2 class="section-title">Awards</h2>
      </div>

      <div class="developers-logo">
        <div class="row">
          <div class="owl-carousel owl-carousel-awards">
            @foreach ($developers as $developer)
              <div class="award-item" style="background-image: url('{{ Voyager::image( $developer->brand ) }}');">
              </div>
            @endforeach
          </div>
        </div>
      </div>

        <div class="rating-wrapper text-center mt-5">
          <h6>House and Condominium in Cebu</h6>
          <div class="rating">
              <p><b>5.0</b></p>
              <div class="stars">
                  <div class="clip-star"></div>
                  <div class="clip-star"></div>
                  <div class="clip-star"></div>
                  <div class="clip-star"></div>
                  <div class="clip-star"></div>
              </div>
          </div>
          <p><b>Real Estate Advisor</b></p>
      </div>
    </div>
  </section>