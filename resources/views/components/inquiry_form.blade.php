<section class="section section-inquiry">
	<div class="container">
		<h3 class="section-title text-center">{{ $title }}</h3>
        @if (session()->has('success_message'))
            <div class="spacer"></div>
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="spacer"></div>
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif


      <form action="{{ route('contact.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="row">
        	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputName">Name *</label>
		          <input type="name" class="form-control" name="name" id="exampleInputName" aria-describedby="emailHelp">
		        </div>
		        <div class="form-group">
		          <label for="exampleInputEmail1">Email address *</label>
		          <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
		        </div>
		        <div class="form-group">
		          <label for="exampleInputPhone">Phone *</label>
		          <input type="phone" class="form-control" name="phone" id="exampleInputPhone">
		        </div>
		        <div class="form-group">
		          <label for="exampleInputPhone">Property *</label>
		          <select name="property" id="" class="form-control">
		            <option value="">Please Select</option>
		            @foreach ($properties as $property)
		              <option value="{{ $property->name }}">{{ $property->name }}</option>
		            @endforeach
		          </select>
		        </div>
        	</div>
        	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleFormControlTextarea1">Message *</label>
		          <textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="6"></textarea>
		        </div>
        	</div>
        	<div class="col-md-12 text-center">
        		<button type="submit" class="btn btn-primary">Submit</button>
        	</div>

        </div>
        
      </form>
	</div>
</section>