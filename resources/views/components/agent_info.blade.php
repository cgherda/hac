<section class="section bg-blue section-agent 
@if (\Route::current()->getName() == 'property') d-sm-block d-md-none @endif">
	<div class="container">
		<h3 class="section-title text-center">{{ $title }}</h3>
		<div class="row">
			<div class="col-md-7">
				<div class="row">
					<div class="col-5 col-md-6 ">
						<div class="agent-info-left">
							<div class="img-agent mb-2">
								<img src="{{ Voyager::image( setting('agent-info.image') ) }}" alt="">
							</div>
							<h2>{{ setting('agent-info.name') }}</h2>
							<h4>{{ setting('agent-info.position') }}</h4>
						</div>
					</div>
					<div class="col-7 col-md-6 pt-2">
						<div class="agent-details">
							<p><i class="fa fa-facebook"></i> <a href="//facebook.com/{{ setting('agent-info.facebook_page') }}" target="_blank" class="fb-link">{{ setting('agent-info.facebook_page') }}</a></p>
							<p><i class="fa fa-mobile"></i> <a href="tel:{{ setting('agent-info.contact_number') }}">{{ setting('agent-info.contact_number') }}</a></p>
							<p><i class="fa fa-whatsapp"></i> <a href="tel:{{ setting('agent-info.contact_number') }}">{{ setting('agent-info.contact_number') }}</a></p>
							<p><i class="fa fa-wechat"></i> <a href="tel:{{ setting('agent-info.contact_number') }}">{{ setting('agent-info.contact_number') }}</a></p>
							<p><i class="fa fa-envelope"></i> <a href="mailto:{{ setting('contact.email') }}">{{ setting('contact.email') }}</a></p>
							<p><span>Brokerage/Company: </span><img src="{{ Voyager::image( setting('agent-info.brokerage') ) }}" style="width: 30%;display: block;" alt=""></p>
						</div>
					</div>
				</div>
			</div>
			@if (\Route::current()->getName() != 'property') 
			<div class="col-md-5">
				
		        @if (session()->has('success_message'))
		            <div class="spacer"></div>
		            <div class="alert alert-success">
		                {{ session()->get('success_message') }}
		            </div>
		        @endif

		        @if(count($errors) > 0)
		            <div class="spacer"></div>
		            <div class="alert alert-danger">
		                <ul>
		                    @foreach ($errors->all() as $error)
		                        <li>{!! $error !!}</li>
		                    @endforeach
		                </ul>
		            </div>
		        @endif
		
				
		      <form action="{{ route('contact.store') }}" method="POST">
		        {{ csrf_field() }}
		        
		        <div class="form-group">
		          <input type="name" class="form-control" name="name" placeholder="Full Name*">
		        </div>
		        <div class="form-group">
		          <input type="email" class="form-control" name="email"  placeholder="Email*">
		        </div>
		        <div class="form-group">
		          <input type="phone" class="form-control" name="phone" placeholder="Phone*">
		        </div>
		        <div class="form-group">
		        	<input type="text" name="location" class="form-control" placeholder="Preffered Location*">
		          {{-- <select name="property" id="" class="form-control">
		            <option value="">Please Select</option>
		            @foreach ($properties as $property)
		              <option value="{{ $property->title }}">{{ $property->title }}</option>
		            @endforeach
		          </select> --}}
		        </div>
		        <div class="form-group">
		          <select name="property_type" id="" class="form-control">
		          	<option value="House & Lot">House & Lot</option>
		          	<option value="Condominium">Condominium</option>
		          </select>
		        </div>
		        <div class="form-group">
		          <textarea class="form-control" name="message" placeholder="Message" rows="3"></textarea>
		        </div>
        		<button type="submit" class="btn btn-lg btn-red">Submit</button>
		        
		      </form>

			</div>
			@endif

			@if (\Route::current()->getName() == 'home')
			<div class="col-md-12 mt-5 sold-properties">
				<h4 class="text-center mb-4">LATEST SOLD PROPERTIES</h4>
				<div class="row">
					{{-- @for ($i = 0; $i < 4; $i++) --}}
					@foreach ($sold_properties as $sold)

					<div class="col-6 col-md-3">
						<div class="card mb-4 box-shadow property-item">
						    <div class="parent" onclick="">
						        <div class="child"  style="background-image: url({{ Voyager::image( $sold->image ) }});">
						        </div>
						        <div class="badge">
						        	SOLD
						        </div>
						    </div>
			                <div class="card-body">
							  <div class="details text-center">
						        <p>{{ $sold->city }}</p>
							  </div>
							  <div class="features">
							  	<p class="name">{{ $sold->title }}</p>
							  </div>
			                </div>
			            </div>
					</div>
					@endforeach
					{{-- @endfor --}}
				</div>
			</div>
			@endif
		</div>
	</div>
</section>