@extends('layouts.master')
@section('content')
  @foreach ($page as $content)
    @section('title', $content->title)
    @section('image', Voyager::image( setting('site.site_image') ))
   <div class="aboutus-banner single-banner"  style="background-image: url('{{ Voyager::image( $content->image ) }}');">
   </div>

  <section class="about-description mt-5">
    <div class="container">
      <div class="text-center">
        <h2 class="section-title">{{ $content->title }}</h2>
      </div>

      <div class="body mb-5">
        {{-- {!! $content->body !!} --}}
        Coming soon. :) 
      </div>

    </div>
{{--     <section class="section blue-section">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-5 col-lg-4">
            <a class="img-wrapper" data-fancybox="gallery" href="{{ Voyager::image($content->image) }}" >
              <img src="{{ Voyager::image($content->image) }}" alt="" class="img-fluid">
            </a>
          </div>
          <div class="col-12 col-md-7 col-lg-8">
            <div class="body">
              {!! $content->body !!}
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-7 col-lg-8">
            <div class="body">
              {!! $content->body !!}
            </div>
          </div>
          <div class="col-12 col-md-5 col-lg-4">
            <a class="img-wrapper" data-fancybox="gallery" href="http://127.0.0.1:8001/images/priland.jpg" >
              <img src="http://127.0.0.1:8001/images/priland.jpg" alt="" class="img-fluid">
            </a>
          </div>
        </div>
      </div>
    </section>
    <section class="section blue-section">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-5 col-lg-4">
            <a class="img-wrapper" data-fancybox="gallery" href="http://127.0.0.1:8001/images/january.jpg" >
              <img src="http://127.0.0.1:8001/images/january.jpg" alt="" class="img-fluid">
            </a>
          </div>
          <div class="col-12 col-md-7 col-lg-8">
            <div class="body">
              {!! $content->body !!}
            </div>
          </div>
        </div>
      </div>
    </section> --}}


  </section>
  @endforeach
@endsection