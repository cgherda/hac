<ul class="navbar-nav ml-auto">
@foreach($items as $menu_item)
    @php
        $isActive = null;
        // Check if link is current
        if(url($menu_item->link()) == url()->current()){
            $isActive = 'active';
        }
        $submenu = $menu_item->children;
    @endphp
	<li class="nav-item {{ $isActive }} {{ (!$menu_item->children->isEmpty() ? "dropdown" : "")}}">
	  <a class="nav-link  {{ (!$menu_item->children->isEmpty() ? "dropdown-toggle" : "")}}" href="{{$menu_item->url}}" @if (!$menu_item->children->isEmpty()) id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" @endif>{{$menu_item->title}}</a>


	    @if(!$menu_item->children->isEmpty())
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          @foreach ($submenu as $item)
	          	<a class="dropdown-item" href="{{$item->url}}">{{$item->title}}</a>
	          @endforeach
	        </div>
	    @endif

	</li>
@endforeach
</ul>